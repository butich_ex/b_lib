#pragma once
#include <string.h>
#include "Basic.h"
#include "Allocator.h"
#include <initializer_list>


template <typename T>
struct Dynamic_Array
{
	T*  data;
	int count;
	int capacity;

	Allocator allocator;

	Dynamic_Array() {}

	Dynamic_Array(int capacity, Allocator allocator)
	{
		this->capacity  = capacity;
		this->allocator = allocator;

		count = 0;
		data = allocate<T>(capacity, allocator);
	}

	inline constexpr static Dynamic_Array empty()
	{
		Dynamic_Array instance;
		instance.data = NULL;
		instance.count = 0;
		instance.capacity = 0;
		instance.allocator = null_allocator;
		return instance;
	}

	Dynamic_Array& operator=(Dynamic_Array const& other) = default;


	template <int N>
	inline static Dynamic_Array from_static_array(const T (&array)[N])
	{
		Dynamic_Array instance;
		instance.data = (T*) array;
		instance.count = N;
		instance.capacity = N;
		instance.allocator = null_allocator;
		return instance;
	}



	T* operator[](int index) const
	{
		return &data[index];
	}

	T* add()
	{
		count += 1;
		ensure_capacity(count);

		T* new_item_location = &data[count - 1];
		return new_item_location;
	}

	T* add(const T item)
	{
		auto item_location = add();
		*item_location = item;
		return item_location;
	}

	int index_of(const T item)
	{
		for (int i = 0; i < count; i++)
		{
			if (data[i] == item)
			{
				return i;
			}
		}
		return -1;
	}

	inline int fast_pointer_index(const T* ptr)
	{
		return ptr - data;
	}

	inline void fast_remove_pointer(T* ptr)
	{
		int index = fast_pointer_index(ptr);
		assert(index >= 0 && index < count);
		remove_at_index(index);
	}

	T* add_at_index(int index, const T item)
    {
        ensure_capacity(count + 1);
        memcpy(data + index + 1, data + index, (count - index) * sizeof(T));
        data[index] = item;
        count += 1;
        return data + index;
    }

	void add_range(const T* item, const uint items_count)
	{
		count += items_count;
		ensure_capacity(count);
		T* new_items_location = &data[count - items_count];
		memcpy(new_items_location, item, sizeof(T) * items_count);
	}

	void remove_at_index(int index)
    {
        count -= 1;
        memcpy(data + index, data + index + 1, (count - index) * sizeof(T));
    }

	void remove_range(int index, int remove_count)
	{
		count -= remove_count;
		memcpy(data + index, data + index + remove_count, (count - index) * sizeof(T));
	}

	bool remove(const T item)
	{
		auto index = index_of(item);
		if (index != -1)
		{
			remove_at_index(index);
			return true;
		}
		return false;
	}

	bool remove_last()
	{
		if (count > 0)
		{
			count -= 1;
			return true;
		}
		return false;
	}

	void clear()
	{
		count = 0;
	}

	void free()
	{
		allocator.free(data, code_location());
		count = 0;
	}

	T pop_last()
	{
		assert(count > 0);

		count -= 1;
		return data[count];
	}

	bool contains(const T item)
	{
		for (int i = 0; i < count; i++)
		{
			if (data[i] == item)
			{
				return true;
			}
		}
		return false;
	}

	void ensure_capacity(int target_capacity)
	{
		if (target_capacity > capacity)
		{
			capacity = capacity * 2;

			data = (T*) allocator.realloc(data, sizeof(T) * capacity / 2, sizeof(T) * capacity, code_location());
		}
	}

	void advance(int length)
	{
		assert(length <= count);
		data += length;
		count -= length;
	}

	Dynamic_Array<T> advanced(int length)
	{
		Dynamic_Array<T> copy = *this;
		copy.advance(length);
		return copy;
	}


	void slice(int start, int length)
	{
		advance(start);
		assert(count >= length);
		count = length;
	}

	Dynamic_Array<T> sliced(int start, int length)
	{
		Dynamic_Array<T> copy = *this;
		copy.slice(start, length);
		return copy;
	}



	inline T* first()
	{
		return data;
	}

	inline T* last()
	{
		return data + (count - 1);
	}

	inline void reverse()
	{
		for (int i = 0; i < (count / 2); i++)
		{
			T temp = data[i];
			data[i] = data[count - 1 - i];
			data[count - 1 - i] = temp;
		}
	}

	// This is for C++11 shorthand for loop
	T* begin()
	{
		return data;
	}

	T* end()
	{
		return data + count;
	}

	Dynamic_Array<T> copy_with(Allocator allocator)
	{
		Dynamic_Array<T> new_array;
		new_array.count = count;
		new_array.capacity = capacity;
		new_array.data = (T*) allocator.alloc(sizeof(T) * capacity, code_location());

		memcpy(new_array.data, data, sizeof(T) * count);

		return new_array;
	}

	Dynamic_Array<T> copy_with_count_as_capacity(Allocator allocator)
	{
		Dynamic_Array<T> new_array;
		new_array.count = count;
		new_array.capacity = count;
		new_array.data = (T*) allocator.alloc(sizeof(T) * count, code_location());

		memcpy(new_array.data, data, sizeof(T) * count);

		return new_array;
	}

	inline void copy_to(Dynamic_Array<T>* array)
	{
		array->ensure_capacity(capacity);
		array->count = count;

		memcpy(array->data, data, sizeof(T) * count);
	}
};



template <typename T>
Dynamic_Array<T> make_array(Allocator allocator, std::initializer_list<T> list)
{
	Dynamic_Array<T> array = Dynamic_Array<T>(list.size(), allocator);

	for (T item : list)
	{
		array.add(item);
	}

	return array;
}


template <typename T>
inline Dynamic_Array<T> make_array(int capacity, Allocator allocator)
{
	return Dynamic_Array<T>(capacity, allocator);
}


template <typename T>
inline constexpr static Dynamic_Array<T> make_const_array(std::initializer_list<T> list)
{
	Dynamic_Array<T> instance;
	instance.data = (T*) list.begin();
	instance.count = list.size();
	instance.capacity = instance.count;
	instance.allocator = null_allocator;
	return instance;
}


