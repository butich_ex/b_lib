#pragma once


#pragma comment(lib, "freetype.lib")

#include "freetype/ft2build.h"
#include FT_FREETYPE_H
#include FT_GLYPH_H

#include "Basic.h"
#include "Array_Map.h"
#include "Hash_Map.h"
#include "Log.h"
#include "Context.h"
#include "File.h"

struct Glyph
{
	char32_t character;
	
	int width;
	int height;

	int freetype_glyph_index;
	int advance;
	int left_offset;
	int top_offset;

	u8* image_buffer;
};

inline FT_Library ft_library;
inline bool is_ft_library_initialized;

struct Font
{
	Unicode_String name;
	Unicode_String file_path;
	Allocator allocator;

	void* file_memory;
	size_t file_size;

	struct Face;
	Dynamic_Array<Face> faces;



	struct Face
	{
		bool is_face_slot_occupied; // For internal usage.

		int size;
		int baseline_offset;
		int line_spacing;
		Allocator allocator;

		Hash_Map<char32_t, Glyph> glyphs;

		FT_Face freetype_face;

		inline Glyph* create_glyph(char32_t character)
		{
			#ifdef TRACY_ENABLE
			ZoneScoped;
			#endif

			int glyph_index = FT_Get_Char_Index(freetype_face, character);

			if (FT_Load_Glyph(freetype_face, glyph_index, FT_LOAD_RENDER))
			{
				log(ctx.logger, U"[Font] Failed to load glyph, index: %", glyph_index);
				return NULL;
			}
			

			Glyph* glyph = glyphs.put(character);

			glyph->character = character;
			glyph->freetype_glyph_index = glyph_index;
			
			glyph->width  = freetype_face->glyph->bitmap.width;
			glyph->height = freetype_face->glyph->bitmap.rows;

			glyph->advance = freetype_face->glyph->advance.x >> 6;

			glyph->left_offset = freetype_face->glyph->bitmap_left;
			glyph->top_offset  = freetype_face->glyph->bitmap_top;

			// Copy bitmap data
			{
				size_t size = glyph->width * glyph->height;
				glyph->image_buffer = (u8*) allocator.alloc(size, code_location());
				memcpy(glyph->image_buffer, freetype_face->glyph->bitmap.buffer, size);
			}

			return glyph;
		}

		// Pointer returned by this may get invalidated during next call to request_glyph.
		inline Glyph* request_glyph(char32_t character)
		{
			Glyph* existing_glyph = glyphs.get(character);

			if (existing_glyph) return existing_glyph;

			return create_glyph(character);
		}
	};



	Face* create_face(int size)
	{
		Face new_face;

#if USE_MEMORY_TO_LOAD_FONT
		FT_Open_Args open_args;
		open_args.flags = FT_OPEN_MEMORY;
		open_args.memory_base = (FT_Byte*) file_memory;
		open_args.memory_size = file_size;
#else
		FT_Open_Args open_args;
		open_args.flags = FT_OPEN_PATHNAME;
		open_args.pathname = file_path.to_utf8(c_allocator, NULL);
#endif

		if (FT_Open_Face(ft_library, &open_args, 0, &new_face.freetype_face) != 0)
		{
			log(ctx.logger, U"Failed to load font: {%} face with size: %", file_path, size);
			return NULL;
		}

		if (FT_Select_Charmap(new_face.freetype_face, FT_ENCODING_UNICODE))
		{
			log(ctx.logger, U"Failed to select unicode charmap for font: %, size: %", name, size);
			return NULL;
		}

		FT_Set_Pixel_Sizes(new_face.freetype_face, 0, size);

		new_face.size = size;
		
		auto metrics = new_face.freetype_face->size->metrics;
		new_face.baseline_offset = (metrics.ascender + metrics.descender) >> 7;
		new_face.line_spacing = metrics.height >> 6;
		new_face.allocator = allocator;

		new_face.glyphs = Hash_Map<char32_t, Glyph>(64, allocator);


		new_face.is_face_slot_occupied = true;

		for (Face& face: faces)
		{
			if (!face.is_face_slot_occupied)
			{
				face = new_face;
				return &face;
			}
		}

		return faces.add(new_face);	
	}

	Face* get_face(int size)
	{
		for (int i = 0; i < faces.count; i++)
		{
			if (faces[i]->size == size)
			{
				return faces[i];
			}
		}
		return create_face(size);
	}


	void unload_face(Font::Face* face)
	{
		int face_index = faces.fast_pointer_index(face);
		assert(face_index >= 0 && face_index < faces.count);


		for (auto& entry: face->glyphs.entries)
		{
			if (!entry.occupied) continue;

			face->allocator.free(entry.value.image_buffer, code_location());
		}
		face->glyphs.free();

		FT_Done_Face(face->freetype_face);

		face->is_face_slot_occupied = false;
	}
};

inline bool load_font(Unicode_String path, Allocator allocator, Font* out_font)
{
	if (!is_ft_library_initialized)
	{
		FT_Error error = FT_Init_FreeType(&ft_library);
		if (error != 0)
		{
			log(ctx.logger, U"Failed to FT_Init_FreeType");
			return false;
		}
		is_ft_library_initialized = true;
	}


#ifdef USE_MEMORY_TO_LOAD_FONT
	File file = open_file(allocator, path, FILE_READ);
	if (!file.succeeded_to_open()) return false;

	size_t size = file.size();
	size_t remaining = size;
	char* mem = (char*) c_allocator.alloc(size, code_location());
	
	while (remaining)
	{
		size_t read_bytes = file.read((u8*) (mem + (size - remaining)), remaining);
		if (read_bytes == 0)
		{
			c_allocator.free(mem, code_location());
			log(ctx.logger, U"Failed to read font file");
			return false;
		}
		remaining -= read_bytes;
	}

	FT_Open_Args open_args;
	open_args.flags = FT_OPEN_MEMORY;
	open_args.memory_base = (FT_Byte*) mem;
	open_args.memory_size = size;
#else
	FT_Open_Args open_args;
	
	open_args.flags = FT_OPEN_PATHNAME;
	open_args.pathname = path.to_utf8(c_allocator, NULL);
#endif


	Font font;

	font.file_path = path.copy_with(c_allocator);
	font.allocator = allocator;
	font.faces = Dynamic_Array<Font::Face>(8, allocator);

//	font.file_memory = mem;
//  font.file_size   = size;
	
	FT_Face face_0;
	if (FT_Open_Face(ft_library, &open_args, 0, &face_0) != 0)
	{
		log(ctx.logger, U"Failed to FT_Open_Face");
		return false;
	}

	const char* name = FT_Get_Postscript_Name(face_0);
	if (name)
	{
		font.name = Unicode_String::from_utf8(name, allocator);
	}
	else
	{
		font.name = get_file_name_without_extension(path).copy_with(allocator);
		log(ctx.logger, U"Font name can't be fetched, falling back to file name based name '%'. Path: '%'", font.name, path);
	}

	log(ctx.logger, U"Loaded font: %", font.name);

	FT_Done_Face(face_0);

	*out_font = font;
	return true;
}




template <typename Char_Type>
struct Glyph_Iterator
{
	Font::Face* face;

	Char_Type previous_char;
	Char_Type current_char;
	int index;

	int x;
	int x_delta;

	Glyph* previous_glyph;
	Glyph* current_glyph;

	bool render_glyph = false;

	Glyph_Iterator() {};

	inline void next_char(Char_Type c)
	{
		ZoneScoped;

		previous_char = current_char;
		current_char = c;

		previous_glyph = current_glyph;
		current_glyph = face->request_glyph(c);

		int old_x = x;
		defer { x_delta = x - old_x; };

		if (c == '\t')
		{
			Glyph* space_glyph = face->request_glyph(' ');

			x += space_glyph->advance * 3;

			render_glyph = false;
		}
		else
		{
			if (index > 0 && false)
			{
				ZoneScopedN("FTGetKerning");
				FT_Vector kerning;

				if (FT_Get_Kerning(face->freetype_face, previous_glyph->freetype_glyph_index, current_glyph->freetype_glyph_index, FT_KERNING_UNFITTED, &kerning) == 0)
				{
					x += kerning.x >> 7;
				}
			}

			x += current_glyph->advance;

			render_glyph = true;
		}
	

		index += 1;
	}

	inline void reset()
	{
		index = 0;
		x = 0;
		x_delta = 0;

		current_glyph = NULL;
		previous_glyph = NULL;
	}
};

template <typename Char_Type>
inline Glyph_Iterator<Char_Type> iterate_glyphs(Font::Face* face)
{
	Glyph_Iterator<Char_Type> iter;

	iter.face = face;
	iter.index = 0;
	iter.x = 0;
	iter.x_delta = 0;

	iter.current_glyph = NULL;
	iter.previous_glyph = NULL;


	return iter;
}

template <typename Char_Type>
struct String_Glyph_Iterator: public Glyph_Iterator<Char_Type>
{
	Template_String<Char_Type> str;


	inline bool next()
	{
		if (this->index >= str.length) return false;

		this->next_char(str[this->index]);

		return true;
	}
};

template <typename Char_Type>
inline String_Glyph_Iterator<Char_Type> string_by_glyphs(Template_String<Char_Type> str, Font::Face* face)
{
	String_Glyph_Iterator<Char_Type> iter;

	Glyph_Iterator<Char_Type> inner_iter = iterate_glyphs<Char_Type>(face);

	memcpy(&iter, &inner_iter, sizeof(inner_iter)); // Hope this works.

	iter.str = str;

	return iter;
}




template <typename Char_Type>
inline int how_many_characters_will_fit(Template_String<Char_Type> str, Font::Face* face, int width)
{
	auto iter = string_by_glyphs(str, face);
	while (iter.next())
	{
		if (iter.x > width)
		{
			return iter.index - 1;
		}
	}
	return str.length;
}

template <typename Char_Type>
inline int pick_appropriate_cursor_position(Template_String<Char_Type> str, Font::Face* face, int width)
{
	auto iter = string_by_glyphs(str, face);
	
	int previous_x = 0;
	while (iter.next())
	{
		if (iter.x > width)
		{
			int middle = (previous_x + iter.x) / 2;
			if (width < middle)
			{
				return max(0, iter.index - 1);
			}
			return iter.index;
		}

		previous_x = iter.x;
	}
	return str.length;
}



template <typename Char_Type>
inline int measure_text_width(Template_String<Char_Type> str, Font::Face* face)
{
	auto iter = string_by_glyphs(str, face);
	while (iter.next()) {}

	return iter.x;
}


#if 0
template <typename Char_Type>
inline Template_String<Char_Type> make_ellipsis(Allocator allocator, Template_String<Char_Type> str, Font::Face* face, int target_width)
{
	auto iter = string_by_glyphs(str, face);

	Glyph* dot_glyph = 

	while (iter.next())
	{
		if (iter.x > target_width)
	}

}
#endif