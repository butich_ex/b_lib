﻿#pragma once

#include "Basic.h"
#include <cstring>
#include "Dynamic_Array.h"
#include <type_traits>
#include "Allocator.h"
#include <ctype.h>

#include "unicode/uchar.h"


inline bool is_whitespace(char32_t c)
{
	return c == U' ' || c == U'\t' || c == U'\r';
}

inline bool is_whitespace(char c)
{
	return c == ' ' || c == '\t' || c == '\r';
}

inline bool is_ascii(char32_t c)
{
	return c <= 255;
}

/*
D:/Typer/Runnable>

dsaasdsd         asasdassdaasd         (((((((((((((((())))))))))))))))))

Bro ""
	sdaasd
	asddsadasasd
	asdasdsadasd
	sadasdadsdssda
\\

*/

template <typename Char_Type>
struct Template_String
{
	inline static constexpr bool is_unicode_string()
	{
		return std::is_same_v<Char_Type, char32_t>;
	}
	inline static constexpr bool is_ascii_string()
	{
		return std::is_same_v<Char_Type, char>;
	}


	static_assert(is_unicode_string() || is_ascii_string(), "Template_String requires Char_Type to be char or utf32_char");

	constexpr static size_t character_size = sizeof(Char_Type);


	Char_Type* data; // This pointer may point to the .text segment so be careful if you want to write into it.
	int length; // Not going to use unsigned version, because it may useful to have length = -1 in some cases


	Template_String() {}

	Template_String(const Char_Type* data, int length)
	{
		this->data   = (Char_Type*) data;
		this->length = length;
	}

	static const Template_String empty;

	template <typename T, int N>
	inline constexpr void set_from_literal(const T (&literal)[N])
	{
		if constexpr (is_ascii_string())
		{
			static_assert(std::is_same_v<T, char>, "String requires const char[] literal");
		}

		if constexpr (is_unicode_string())
		{
			static_assert(std::is_same_v<T, char32_t>, "Unicode_String requires const char32_t[] literal");
		}		
		data = (Char_Type*) (literal);
		length = N - 1;
	}

	template <typename T, int N>
	Template_String(const T (&literal)[N])
	{
		set_from_literal(literal);
	}
	template <typename T, int N>
	Template_String& operator=(const T (&literal)[N])
	{
		set_from_literal(literal);
		return *this;
	}

	inline static Template_String from_char(Char_Type c, Allocator allocator)
	{
		Template_String str;
		str.data = (Char_Type*) allocator.alloc(sizeof(Char_Type), code_location());
		str.length = 1;

		memcpy(str.data, &c, sizeof(Char_Type));

		return str;
	}


	inline static Template_String from_c_string(char* c_str, Allocator allocator)
	{
		static_assert(is_ascii_string());
		
		Template_String str;

		str.length = strlen(c_str);
		if (str.length == 0)
		{
			return empty;
		}

		str.data = (char*) allocator.alloc(str.length, code_location());

		memcpy(str.data, c_str, str.length);

		return str;
	}


	inline static Template_String from_ascii(Template_String<char> ascii_str, Allocator allocator)
	{
		static_assert(is_unicode_string());

		Template_String str;
		str.data = (char32_t*) allocator.alloc(sizeof(char32_t) * ascii_str.length, code_location());
		str.length = ascii_str.length;

		for (int i = 0; i < str.length; i++)
		{
			str.data[i] = ascii_str.data[i];
		}

		return str;
	}

	inline static Template_String from_utf16(const char16_t* utf16_str, Allocator allocator, int max_buffer_size = INT_MAX)
	{
		static_assert(is_unicode_string());

		int length = 0;

		const char16_t* ptr = utf16_str;
		while (true)
		{
			char16_t c = *ptr;
			ptr += 1;
			
			if (c == 0) break; // zero terminator
			if ((u8*) ptr > (u8*) utf16_str + max_buffer_size)
			{
				break;
			}

			if ((c & 0b1111'1100'0000'0000) == 0b1101'1000'0000'0000)
			{
				// We know that this code point is encoded with 2 16bit code units.
				c = *ptr;
				ptr += 1;

				if (((c & 0b1111'1100'0000'0000) != 0b1101'1100'0000'0000))
				{
					// Invalid UTF16 sequence.
					assert(false);
				}
			}
			
			length += 1;
		}


		Template_String str;
		str.data = (char32_t*) allocator.alloc(sizeof(char32_t) * length, code_location());
		str.length = length;


		ptr = utf16_str;
		for (int i = 0; i < length; i++)
		{
			char16_t c = *ptr;
			ptr += 1;

			if ((c & 0b1111'1100'0000'0000) == 0b1101'1000'0000'0000)
			{
				char32_t utf32_c;

				utf32_c = (c - 0xD800) << 10;

				c = *ptr;
				ptr += 1;

				char32_t bruh = ((char32_t) c) - 0xDC00;
				utf32_c |= bruh;
				utf32_c += 0x10000;

				str.data[i] = utf32_c;
			}
			else
			{
				str.data[i] = c;
			}
		}
		
		return str;
	}

	inline static Template_String from_utf8(const char* utf8_str, Allocator allocator, int max_buffer_size = INT_MAX)
	{
		static_assert(is_unicode_string());

		int length = 0;
		
		u8* ptr = (u8*) utf8_str;
		while (true)
		{
			char c = *ptr;
			ptr += 1;
			
			if (c == '\0') break;
			if (ptr > (u8*) utf8_str + max_buffer_size)
			{
				break;
			}

			// Continuation detected
			if ((c & 0b1100'0000) == 0b1000'0000)
				continue;

			length += 1;
		}

		char32_t* data = (char32_t*) allocator.alloc(length * sizeof(char32_t), code_location());

		ptr = (u8*) utf8_str;

		for (int i = 0; i < length; i++)
		{
			char32_t codepoint = 0;

			char c = *ptr;
			ptr += 1;

			if ((c & 0b1000'0000) == 0b0000'0000)
			{
				codepoint |= c & 0b0111'1111;
			}
			else if ((c & 0b1110'0000) == 0b1100'0000)
			{
				codepoint |= (c & 0b0001'1111) << 6;

				c = *ptr;
				codepoint |= c & 0b0011'1111;

				ptr += 1;
			}
			else if ((c & 0b1111'0000) == 0b1110'0000)
			{
				codepoint |= (c & 0b0000'1111) << 12;

				c = *ptr;
				codepoint |= (c & 0b0011'1111) << 6;

				ptr += 1;

				c = *ptr;
				codepoint |= c & 0b0011'1111;

				ptr += 1;
			}
			else if ((c & 0b1111'1000) == 0b1111'0000)
			{
				codepoint |= (c & 0b0000'0111) << 18;

				c = *ptr;
				codepoint |= (c & 0b0011'1111) << 12;

				ptr += 1;

				c = *ptr;
				codepoint |= (c & 0b0011'1111) << 6;

				ptr += 1;

				c = *ptr;
				codepoint |= c & 0b0011'1111;

				ptr += 1;

			}
			else
			{
				assert(false);
			}


			data[i] = codepoint;
		}

		Template_String string;
		string.data = data;
		string.length = length;

		return string;
	}

	// This will work for both: ASCII and UNICODE strings.
	// Zero-terminates output string
	inline char16_t* to_utf16(Allocator allocator, int* out_utf16_length)
	{
		if constexpr (is_ascii_string())
		{
			char16_t* str = (char16_t*) allocator.alloc(sizeof(char16_t) * (length + 1), code_location());

			for (int i = 0; i < length; i++)
			{
				str[i] = (char16_t) data[i];
			}

			str[length] = 0;

			if (out_utf16_length)
				*out_utf16_length = length;
			
			return str;
		}



		int utf16_length = 0;

		for (int i = 0; i < length; i++)
		{
			Char_Type c = data[i];

			if (c > 0x10000)
				utf16_length += 2;
			else
				utf16_length += 1;
		}

		char16_t* str = (char16_t*) allocator.alloc(sizeof(char16_t) * (utf16_length + 1), code_location());

		char16_t* ptr = str;
		for (int i = 0; i < length; i++)
		{
			Char_Type c = data[i];

			if (c > 0x10000)
			{
				u32 code_point = c - 0x10000;


				*ptr = ((code_point >> 10) + 0xD800);
				ptr += 1;

				*ptr = ((code_point & 0b0000'0011'1111'1111) + 0xDC00);
			}
			else
			{
				*ptr = c;
			}

			ptr += 1;
		}

		*ptr = 0;

		if (out_utf16_length)
			*out_utf16_length = length;
			
		return str;
	}

	// This will work for both: ASCII and UNICODE strings.
	// Zero-terminates output string
	inline char* to_utf8(Allocator allocator, int* out_utf8_length)
	{
		if constexpr (is_ascii_string())
		{
			char* str = (char*) allocator.alloc(length + 1, code_location());
			memcpy(str, data, length);

			str[length] = 0;

			if (out_utf8_length)
				*out_utf8_length = length;

			return str;
		}
		

		int utf8_length = 0;

		for (int i = 0; i < length; i++)
		{
			Char_Type c = data[i];

			if (c < 0x007F)
				utf8_length += 1;
			else if (c < 0x07FF)
				utf8_length += 2;
			else if (c < 0xFFFF)
				utf8_length += 3;
			else
				utf8_length += 4;
		}

		char* str = (char*) allocator.alloc(utf8_length + 1, code_location());

		char* ptr = str;
		for (int i = 0; i < length; i++)
		{
			Char_Type c = data[i];

			if (c <= 0x007F)
			{
				*ptr = c;
				ptr += 1;
			}
			else if (c <= 0x07FF)
			{
				ptr[1] = (( c        & 0b0011'1111) | 0b1000'0000);
				ptr[0] = (((c >> 6)  & 0b0001'1111) | 0b1100'0000);

				ptr += 2;
			}
			else if (c <= 0xFFFF)
			{
				ptr[2] = (( c        & 0b0011'1111) | 0b1000'0000);
				ptr[1] = (((c >> 6)  & 0b0011'1111) | 0b1000'0000);
				ptr[0] = (((c >> 12) & 0b0000'1111) | 0b1110'0000);

				ptr += 3;
			}
			else
			{
				ptr[3] = (( c        & 0b0011'1111) | 0b1000'0000);
				ptr[2] = (((c >> 6)  & 0b0011'1111) | 0b1000'0000);
				ptr[1] = (((c >> 12) & 0b0011'1111) | 0b1000'0000);
				ptr[0] = (((c >> 24) & 0b0000'0111) | 0b1111'0000);

				ptr += 3;
			}
		}

		str[utf8_length] = 0;

		if (out_utf8_length)
			*out_utf8_length = utf8_length;

		return str;
	}

	// @TODO: rename this @CLeanup
	inline Template_String<char> to_utf8_but_ascii(Allocator allocator)
	{
		int utf8_length;
		char* utf8_data = to_utf8(allocator, &utf8_length);

		return Template_String<char>(utf8_data, utf8_length);
	}


	inline Template_String<char32_t> to_unicode_string(Allocator allocator)
	{
		static_assert(is_ascii_string());

		Template_String<char32_t> str;

		str.data = (char32_t*) allocator.alloc(sizeof(char32_t) * length, code_location());	
		str.length = length;

		for (int i = 0; i < length; i++)
		{
			str.data[i] = data[i];
		}

		return str;
	}

	inline Template_String to_lower_case(Allocator allocator)
	{
		static_assert(is_unicode_string());

		Template_String<char32_t> str;

		str.data = (char32_t*) allocator.alloc(sizeof(char32_t) * length, code_location());
		str.length = length;

		for (int i = 0; i < length; i++)
		{
			str.data[i] = u_tolower(data[i]);
		}

		return str;
	}

	inline Template_String to_upper_case(Allocator allocator)
	{
		static_assert(is_unicode_string());

		Template_String<char32_t> str;

		str.data = (char32_t*) allocator.alloc(sizeof(char32_t) * length, code_location());
		str.length = length;

		for (int i = 0; i < length; i++)
		{
			str.data[i] = u_toupper(data[i]);
		}

		return str;
	}


	bool operator==(Template_String other) const
	{
		if (other.length != length) return false;
		if (other.data == data)     return true;

		for (int i = 0; i < length; i++)
		{
			if (other.data[i] != data[i])
				return false;
		}
		return true;
	}
	bool operator!=(Template_String other) const
	{
		return !this->operator==(other);
	}

	Char_Type operator[](int index) const
	{
		return data[index];
	}

	void advance(int advance_length)
	{
		assert(advance_length >= 0 && advance_length <= length);
		data   += advance_length;
		length -= advance_length;
	}
	void slice(int slice_start, int slice_length)
	{
		assert(slice_length <= length - slice_start);
		data   = data + slice_start;
		length = slice_length;
	}
	Template_String advanced(int advance_length) const
	{
		Template_String copy = *this;
		copy.advance(advance_length);
		return copy;
	}
	Template_String sliced(int start, int length) const
	{
		Template_String copy = *this;
		copy.slice(start, length);
		return copy;
	}


	bool is_not_empty() const
	{
		return length != 0;
	}
	bool is_empty() const
	{
		return length == 0;
	}
	bool is_blank() const
	{
		for (int i = 0; i < length; i++)
		{
			if (!is_whitespace(data[i]))
				return false;
		}
		return true;
	}
	bool is_not_blank() const
	{
		return !is_blank();
	}


	bool starts_with(Template_String other) const
	{
		if (other.length > length) return false;

		for (int i = 0; i < other.length; i++)
		{
			if (data[i] != other.data[i])
				return false;
		}
		return true;
	}
	bool ends_with(Template_String other) const
	{
		int length_diff = length - other.length;
		if (length_diff < 0) return false;

		for (int i = 0; i < other.length; i++)
		{
			if (data[i + length_diff] != other.data[i])
				return false;
		}
		return true;
	}
	bool ends_with(Char_Type c) const
	{
		if (length < 1) return false;
		return data[length - 1] == c;
	}

	bool advance_if_starts_with(Template_String other)
	{
		if (starts_with(other))
		{
			advance(other.length);
			return true;
		}

		return false;
	}


	void remove(int remove_start, int remove_length)
	{
		assert(remove_start + remove_length <= length);

		auto remaining = length - (remove_start + remove_length);
		memcpy(data + remove_start, data + remove_start + remove_length, remaining * character_size);
		length -= remove_length;
	}

	int index_of(Char_Type c, int start_index = 0) const
	{
		for (int i = start_index; i < length; i++)
		{
			if (data[i] == c)
			{
				return i;
			}
		}
		return -1;
	}
	
	void trim_start()
	{
		for (int i = 0; i < length; i++)
		{
			char32_t c = data[i];

			if (!is_whitespace(c))
			{
				advance(i);
				return;
			}
		}

		advance(length);
	}
	void trim_end()
	{
		for (int i = length - 1; i >= 0; i--)
		{
			char32_t c = data[i];

			if (!is_whitespace(c))
			{
				slice(0, i + 1);
				return;
			}
		}

		slice(0, 0);
	}
	void trim()
	{
		trim_start();
		trim_end();
	}
	Template_String trimmed_start() const
	{
		Template_String copy = *this;
		copy.trim_start();
		return copy;
	}
	Template_String trimmed_end() const
	{
		Template_String copy = *this;
		copy.trim_end();
		return copy;
	}
	Template_String trimmed() const
	{
		Template_String copy = *this;
		copy.trim();
		return copy;
	}

	// @TODO: remove ASCII version from here
	void to_lower_case()
	{
		static_assert(is_ascii_string());

		for (int i = 0; i < length; i++)
		{
			data[i] = tolower(data[i]);
		}
	}
	void to_upper_case()
	{
		static_assert(is_ascii_string());

		for (int i = 0; i < length; i++)
		{
			data[i] = toupper(data[i]);
		}
	}

	Template_String copy_with(Allocator allocator) const
	{
		Template_String new_string;

		new_string.data = (Char_Type*) allocator.alloc(length * character_size, code_location());
		new_string.length = length;

		memcpy(new_string.data, data, length * character_size);
		
		return new_string;
	}

	char* to_c_string(Allocator allocator) const
	{
		static_assert(is_ascii_string());

		char* c_string = (char*) allocator.alloc((length + 1) * sizeof(char), code_location());
		
		memcpy(c_string, data, length * sizeof(char));
		c_string[length] = '\0';

		return c_string;
	}

	inline long hash() const
	{
		long hash = 3423243;
		for (int i = 0; i < length; i++)
		{
			hash ^= data[i];
		}
		return hash;
	}




	inline Char_Type* begin()
	{
		return data;
	};

	inline Char_Type* end()
	{
		return data + length;
	}
};

template <typename T>
const Template_String<T> Template_String<T>::empty = Template_String<T>(NULL, 0);



using         String = Template_String<char>;

// UTF-32
using Unicode_String = Template_String<char32_t>;


template <int N>
Template_String(const char (&)[N]) -> Template_String<char>;


Template_String(const char* sd) -> Template_String<char>;

template <int N>
Template_String(const char32_t (&)[N]) -> Template_String<char32_t>; // C++17 feature!!


inline void test_strings_compile_time_check_and_misc()
{
	auto unicode_str = Template_String(U"sdassdsdasda");
	auto ascii_str = String("sdassdsdasda");

	String empty_ascii_str = String::empty;


	// @ToTest 
	// Template_String<int> == Template_String<float> or Template_String<char> == Template_String<utf32_char>
}



inline bool compare_strings_case_independently(String a, String b)
{
	if (a.length != b.length) return false;
	if (a.data == b.data) return true;

	for (int i = 0; i < a.length; i++)
	{
		if (tolower(a[i]) != tolower(b[i])) return false;
	}

	return true;
}


template <typename Char_Type>
inline Template_String<Char_Type> skip_whitespace(Template_String<Char_Type> str)
{
	for (int i = 0; i < str.length; i++)
	{
		Char_Type c = str[i];

		if (!is_whitespace(c))
		{
			return str.advanced(i);
		}
	}

	return Template_String<Char_Type>::empty;
}

template <typename Char_Type>
inline Template_String<Char_Type> take_until_whitespace(Template_String<Char_Type> str)
{
	for (int i = 0; i < str.length; i++)
	{
		Char_Type c = str[i];

		if (is_whitespace(c))
		{
			return str.sliced(0, i);
		}
	}

	return str;
}


template <typename Char_Type>
inline Template_String<Char_Type> take_until(Template_String<Char_Type> str, char32_t until)
{
	for (int i = 0; i < str.length; i++)
	{
		Char_Type c = str[i];

		if (c == until)
		{
			return str.sliced(0, i);
		}
	}

	return str;
}




template <typename Char_Type>
Dynamic_Array<Template_String<Char_Type>> split(Template_String<Char_Type> to_split, Char_Type by, Allocator allocator, bool copy_strings = false)
{
	Dynamic_Array<Template_String<Char_Type>> strings = Dynamic_Array<Template_String<Char_Type>>(4, allocator);

	auto add_string = [&](int last_split_index, int ending_index_exclusive)
	{
		Template_String<Char_Type> new_string;
		new_string.length = ending_index_exclusive - last_split_index - 1;
		new_string.data = &to_split.data[last_split_index + 1];

		if (copy_strings)
		{
			new_string = new_string.copy_with(allocator);
		}

		strings.add(new_string);
	};
	
	int last_split_index = -1;
	for (int i = 0; i < to_split.length; i++)
	{
		auto c = to_split.data[i];
		if (c == by)
		{
			add_string(last_split_index, i);
			last_split_index = i;
		}
	}

	if ((last_split_index != to_split.length - 1) || strings.count == 0)
	{
		add_string(last_split_index, to_split.length);
	}

	return strings;
}


template <typename Char_Type>
Dynamic_Array<Template_String<Char_Type>> split(Template_String<Char_Type> to_split, Dynamic_Array<Char_Type> by, Allocator allocator, bool copy_strings = false)
{
	Dynamic_Array<Template_String<Char_Type>> strings = Dynamic_Array<Template_String<Char_Type>>(4, allocator);

	auto add_string = [&](int last_split_index, int ending_index_exclusive)
	{
		Template_String<Char_Type> new_string;
		new_string.length = ending_index_exclusive - last_split_index - 1;
		new_string.data = &to_split.data[last_split_index + 1];

		if (copy_strings)
		{
			new_string = new_string.copy_with(allocator);
		}

		strings.add(new_string);
	};
	
	int last_split_index = -1;
	for (int i = 0; i < to_split.length; i++)
	{
		auto c = to_split.data[i];
		if (by.contains(c))
		{
			add_string(last_split_index, i);
			last_split_index = i;
		}
	}

	if ((last_split_index != to_split.length - 1) || strings.count == 0)
	{
		add_string(last_split_index, to_split.length);
	}

	return strings;
}