#pragma once

#include "Basic.h"
#include "Allocator.h"


template <typename K, typename T>
struct Hash_Map
{
	struct Entry
	{
		K key;
		T value;
		bool occupied;
	};

	Dynamic_Array<Entry> entries; // .add is not used, array is just reallocated in case of a rehash.
	Allocator allocator;
	float resize_factor;




	u64 hash_func(K key)
	{
		// Wtf is this???
		u64 hash = 1;

		u8* byte_ptr = (u8*) &key;

		for (int i = 0; i < sizeof(K); i++)
		{
			hash >> i;
			hash ^= byte_ptr[i];
		}

		return hash;
	}


	Hash_Map() {}

	Hash_Map(int capacity, Allocator allocator, float resize_factor = 0.75)
	{
		this->allocator = allocator;
		this->resize_factor = resize_factor;

		entries = make_array<Entry>(capacity, allocator);
		memset(entries.data, 0, entries.capacity * sizeof(Entry));
	}


	T* put(const K key)
	{
		if (scale(entries.capacity, resize_factor) <= entries.count)
		{
			grow_and_rehash();
		}

		u64 hash = hash_func(key);

		int index = hash % entries.capacity;

		// Quadratic probing? Right?
		int collisions = 0;
		while (true)
		{
			Entry* entry = entries[index];
			if (entry->occupied) 
			{
				index += 2 * (++collisions) - 1;
				if (index >= entries.capacity)
					index = index - entries.capacity;

				continue;
			}

			entry->key = key;
			entry->occupied = true;

			entries.count += 1;

			return &entry->value;
		}
	}

	T* get(const K key)
	{
		u64 hash = hash_func(key);

		int index = hash % entries.capacity;

		// :CopyPaste: from put method above.
		// Quadratic probing? Right?
		int collisions = 0;
		while (true)
		{
			Entry* entry = entries[index];

			if (!entry->occupied) return false;

			if (entry->key != key) 
			{
				index += 2 * (++collisions) - 1;
				if (index >= entries.capacity)
					index = index - entries.capacity;

				continue;
			}

			return &entries[index]->value;
		}
	}

	bool remove(const K key)
    {
    	// @Hack or not??
    	T*   value = get(key);
    	if (!value) return false;

    	Entry* entry = add_bytes_to_pointer(value, offsetof(Entry, value));

    	entry->occupied = false;

    	entries.count -= 1;
    }

	// Pointer will get invalid on the event of resize.
	T* put(const K key, const T item)
	{
		T* ptr = put(key);
		*ptr = item;
		return ptr;
	}


	void grow_and_rehash()
	{
		auto old_entries = entries;
		defer { old_entries.free(); };
		entries = make_array<Entry>(entries.capacity * 2, allocator);
		memset(entries.data, 0, entries.capacity * sizeof(Entry));
		
		for (Entry& entry: old_entries)
		{
			if (!entry.occupied) continue;

			put(entry.key, entry.value);
		}
	}

	void free()
	{
		entries.free();
	}
};