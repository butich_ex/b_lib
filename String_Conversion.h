#pragma once

#include "Basic.h"
#include "Allocator.h"
#include "String.h"
#include <type_traits>
#include <limits>
#include "Reflection.h"
#include "String_Builder.h"


template <typename T, int base = 10>
inline void print_integer_number_to_char_buffer_reversed(T num, char* buffer, u32& position)
{
	static_assert(sizeof(T) == 1 || sizeof(T) == 2 || sizeof(T) == 4 || sizeof(T) == 8, "Integer size is not supported");
	static_assert(base == 2 || base == 10 || base == 16, "Supported integer bases: 2, 8, 10, 16");

	constexpr bool is_signed = std::numeric_limits<T>::is_signed && base == 10;

	using Number_T = typename std::conditional<base == 10, T, std::make_unsigned<T>::type>::type;

	union
	{
		Number_T number;
		T number_alias;
	};
	number_alias = num;


	u32 start_position = position;

	bool negative = false;

	if (number == 0)
	{
		buffer[position] = '0';
		position += 1;
	}
	else
	{
		if constexpr (is_signed)
		{
			if (number < 0)
			{
				negative = true;
			}
		}
		while (number != 0)
		{
			s32 digit = number % base;
			number /= base;

			if (negative)
			{
				digit = -digit;
			}

			if constexpr (base == 16)
			{
				if (digit > 9)
				{
					buffer[position] = 'A' + digit - 10;
				}
				else
				{
					buffer[position] = '0' + digit;
				}
			}
			else
			{
				buffer[position] = '0' + digit;
			}

			position += 1;
		}
	}



	// Fill up 0s
	if constexpr (base == 2 || base == 16)
	{
		auto bits_per_digit = [](int number_base)
		{
			switch (number_base)
			{
				case 2:  return 1;
				case 16: return 4;
			}
			return 0;
		};

		constexpr s32 digits_count_per_decimal_digit = bits_per_digit(base);

		constexpr s32 total_bits = sizeof(T) * 8;
		constexpr s32 total_digits_count = total_bits / digits_count_per_decimal_digit;

		s32 fill_count = total_digits_count - (position - start_position);

		for (int i = 0; i < fill_count; i++)
		{
			buffer[position + i] = '0';
		}
		position += fill_count;
	}


	if constexpr (base == 2)
	{
		buffer[position]     = 'b';
		buffer[position + 1] = '0';
		position += 2;
	}
	else if constexpr (base == 16)
	{
		buffer[position]     = 'x';
		buffer[position + 1] = '0';
		position += 2;
	}

	if constexpr (is_signed)
	{
		if (negative)
		{
			buffer[position] = '-';
			position += 1;
		}
	}
}

template <typename T, int base = 10>
inline void print_integer_number_to_char_buffer(T number, char* buffer, u32& position)
{
	int start_position = position;

	print_integer_number_to_char_buffer_reversed<T, base>(number, buffer, position);

	reverse(buffer, position - start_position);
}


// No sign will be printed unless base = 10.
template <typename T, int base = 10, std::enable_if_t<std::numeric_limits<T>::is_integer, bool> = 0>
inline String to_string(T num, Allocator allocator)
{
	static_assert(sizeof(T) <= 64 / 8);
	static_assert(std::numeric_limits<T>::is_integer);

	// 64 is max count of characters in 64 number with base of 2. 1 for sign. 2 is for prefixes: '0b' or '0x'
	//    Just allocate 128 bytes to be sure nothing bad happens.
	char* buffer = (char*) allocator.alloc(/*64 + 1 + 2*/ 128, code_location());

	u32 position = 0;

	print_integer_number_to_char_buffer<T, base>(num, buffer, position);

	return String(buffer, position);
}

template <typename T>
inline String float_to_string_internal_impl(T num, Allocator allocator, int max_decimal_digits = 99999999)
{
	// Using implementation from:
	// https://blog.benoitblanchon.fr/lightweight-float-to-string/


	// 1 - sign char;
	// 3 - exponent string length;
	// 10 - u32 max string lennth;
	const u32 buffer_size = 1 + 3 + 10 + 10;
	char* buffer = (char*) allocator.alloc(buffer_size, code_location());
	u32 position = 0;

	if (num == 0) // Negative zero will be caught here.
	{
		if (signbit(num))
		{
			memcpy(buffer, "-0", 2);
			return String(buffer, 2);
		}
		else
		{
			memcpy(buffer, "0", 1);
			return String(buffer, 1);
		}
	}
	else if (isnan(num))
	{
		if (signbit(num))
		{
			memcpy(buffer, "-NaN", 4);
			return String(buffer, 4);
		}
		else
		{
			memcpy(buffer, "NaN", 3);
			return String(buffer, 3);
		}
	}
	else if (num == INFINITY)
	{
		memcpy(buffer, "Inf", 3);
		return String(buffer, 3);
	}
	else if (num == -INFINITY)
	{
		memcpy(buffer, "-Inf", 4);
		return String(buffer, 4);
	}

	bool negative = false;

	if (num < 0)
	{
		negative = true;
		num = -num;
	}


	int exponent = 0;

	{
    	const double positive_exp_threshold = 1e7;
		const double negative_exp_threshold = 1e-5;

		if (num >= positive_exp_threshold)
		{
			if (num >= 1e256)
			{
		  		num /= 1e256;
		  		exponent += 256;
			}
			if (num >= 1e128)
			{
				num /= 1e128;
			 	exponent += 128;
			}
			if (num >= 1e64)
			{
				num /= 1e64;
				exponent += 64;
			}
			if (num >= 1e32)
			{
				num /= 1e32;
				exponent += 32;
			}
			if (num >= 1e16)
			{
				num /= 1e16;
				exponent += 16;
			}
			if (num >= 1e8)
			{
				num /= 1e8;
				exponent += 8;
			}
			if (num >= 1e4)
			{
				num /= 1e4;
				exponent += 4;
			}
			if (num >= 1e2)
			{
				num /= 1e2;
				exponent += 2;
			}
			if (num >= 1e1)
			{
				num /= 1e1;
			 	exponent += 1;
			}
		}
		else if (num <= negative_exp_threshold)
		{
			if (num < 1e-255)
			{
				num *= 1e256;
				exponent -= 256;
			}
			if (num < 1e-127)
			{
				num *= 1e128;
				exponent -= 128;
			}
			if (num < 1e-63)
			{
				num *= 1e64;
				exponent -= 64;
			}
			if (num < 1e-31)
			{
				num *= 1e32;
				exponent -= 32;
			}
			if (num < 1e-15)
			{
				num *= 1e16;
				exponent -= 16;
			}
			if (num < 1e-7)
			{
				num *= 1e8;
				exponent -= 8;
			}
			if (num < 1e-3)
			{
				num *= 1e4;
				exponent -= 4;
			}
			if (num < 1e-1)
			{
				num *= 1e2;
				exponent -= 2;
			}
			if (num < 1e0)
			{
			 	num *= 1e1;
				exponent -= 1;
			}
		}
	}

	u32 integral_part = (u32) num;
	float remainder = (num - integral_part);
	u32 decimal_part  = (u32) (remainder * 1e9);

	// @Feature @Robustness: maybe do rounding????

	// Doing everything in reverse order

	if (exponent != 0)
	{
		print_integer_number_to_char_buffer_reversed(abs(exponent), buffer, position);

		if (exponent < 0)
		{
			buffer[position] = '-';
			position += 1;
		}

		buffer[position] = 'e';
		position += 1;
	}

	if (decimal_part != 0)
	{
		if (max_decimal_digits > 0)
		{
			int start_position = position;

			// Example.
			// This will turn 1234.5600000 to 1234.56
			while (decimal_part % 10 == 0)
			{
				decimal_part /= 10;
			}

			print_integer_number_to_char_buffer_reversed(decimal_part, buffer, position);

			int decimal_digits_count = position - start_position;

			if (decimal_digits_count > max_decimal_digits)
			{
				memcpy(&buffer[start_position], &buffer[position - max_decimal_digits], max_decimal_digits);
				position = start_position + max_decimal_digits;
			}

			buffer[position] = '.';
			position += 1;
		}
	}

	print_integer_number_to_char_buffer_reversed(integral_part, buffer, position);

	if (negative)
	{
		buffer[position] = '-';
		position += 1;
	}

	u32 length = position;

	reverse(buffer, length);

	return String(buffer, length);
}

inline String to_string(float num, Allocator allocator, int max_decimal_digits = 99999999)
{
	return float_to_string_internal_impl(num, allocator, max_decimal_digits);
}
inline String to_string(double num, Allocator allocator, int max_decimal_digits = 99999999)
{
	return float_to_string_internal_impl(num, allocator, max_decimal_digits);
}

inline String to_string(bool b, Allocator allocator)
{
	String str;

	if (b)
		str = String("true");
	else
		str = String("false");

	return str.copy_with(allocator);
}


template <typename T>
inline String to_string_filled_with_zeros_in_front(T thing, int target_digits_count, Allocator allocator)
{
	String str = to_string(thing, allocator);

	if (str.length >= target_digits_count) return str;

	int count_to_fill = target_digits_count - str.length;

	char* buffer = (char*) allocator.alloc(target_digits_count, code_location());
	memcpy(buffer + count_to_fill, str.data, str.length);

	for (int i = 0; i < count_to_fill; i++)
	{
		buffer[i] = '0';
	}

	allocator.free(str.data, code_location());

	return String(buffer, target_digits_count);
}




inline String primitive_to_string(Reflection::Primitive_Type* primitive_type, void* mem, Allocator allocator)
{
	using namespace Reflection;


	switch (primitive_type->primitive_kind)
	{
		#define handle_primitive_type(a) case Primitive_Kind::P_##a:\
		{\
			return to_string(*( a*) mem, allocator);\
		}\
		break;\

		handle_primitive_type(u8);
		handle_primitive_type(s8);
		handle_primitive_type(u16);
		handle_primitive_type(s16);
		handle_primitive_type(u32);
		handle_primitive_type(s32);
		handle_primitive_type(u64);
		handle_primitive_type(s64);

		handle_primitive_type(f32);
		handle_primitive_type(f64);

		handle_primitive_type(bool);

		default: assert(false);
	}

	return String::empty;
}

// The difference between this and write_thing.. is that you're not supposed to be able to parse
//  thing printed by this routine back. Use it for printing values, it's for this case than write_thing.
inline void print_thing_to_builder(Reflection::Type* type, void* mem, String_Builder<char>* builder, Allocator allocator, int inheritance, bool dereference_pointers = false, bool print_struct_name = true)
{
	using namespace Reflection;

	auto append_indentation = [&](int indentation)
	{
		for (int i = 0; i < indentation; i++)
		{
		#if 1
			builder->append("    ");
		#else
			builder->append("\t");
		#endif
		}
	};


	switch (type->kind)
	{
		case Type_Kind::Array:
		{
			Array_Type* array_type = (Array_Type*) type;

			builder->append("{\n");

			Dynamic_Array<int>* array = (Dynamic_Array<int>*) mem;

			int item_size = array_type->inner_type->size;

			for (int i = 0; i < array->count; i++)
			{
				append_indentation(inheritance + 1);
				print_thing_to_builder(array_type->inner_type, add_bytes_to_pointer(array->data, i * item_size), builder, allocator, inheritance + 1, dereference_pointers, print_struct_name);

				if (i != array->count - 1)
				{
					builder->append(",");
				}
				builder->append("\n");
			}

			append_indentation(inheritance);

			builder->append("}\n");
		}
		break;

		case Type_Kind::Struct:
		{
			Struct_Type* struct_type = (Struct_Type*)type;

			if (print_struct_name)
			{
				builder->append(struct_type->name);
				builder->append(" :: struct\n");
			}
			
			append_indentation(inheritance);
			builder->append("{\n");

			for (auto iter = struct_type->iterate_members(allocator); Struct_Member* member = iter.next();)
			{
				append_indentation(inheritance + 1);

				builder->append(member->name);
				builder->append(": ");

				if (member->type->kind == Type_Kind::Pointer)
				{
					int indirection_level;
					Type* inner_type = get_pointer_inner_type_with_indirection_level((Pointer_Type*) member->type, &indirection_level);

					builder->append(inner_type->name);
					for (int i = 0; i < indirection_level; i++)
					{
						builder->append("*");
					}
				}
				else
				{
					builder->append(member->type->name);
				}
				builder->append(" = ");

				print_thing_to_builder(member->type, add_bytes_to_pointer(mem, member->offset), builder, allocator, inheritance + 1, dereference_pointers);

				builder->append("\n");
			}

			append_indentation(inheritance);
			builder->append("}");
		}
		break;

		case Type_Kind::Pointer:
		{
			Pointer_Type* pointer_type = (Pointer_Type*)type;

			int indirection_level;
			Type* inner_type = get_pointer_inner_type_with_indirection_level(pointer_type, &indirection_level);

			String pointer_value_string;

			switch (pointer_type->size)
			{
				case 8:
				{
					u64 value;
					memcpy(&value, mem, sizeof(value));

					pointer_value_string = to_string<u64, 16>(value, allocator);
				}
				break;

				case 4:
				{
					u32 value;
					memcpy(&value, mem, sizeof(value));

					pointer_value_string = to_string<u32, 16>(value, allocator);
				}
				break;

				default: assert(false);
			}

			builder->append(pointer_value_string);

			allocator.free(pointer_value_string.data, code_location());


			if (dereference_pointers && mem != NULL && indirection_level == 1)
			{
				void** ptr = (void**) mem;

				builder->append(" = ");
				print_thing_to_builder(inner_type, *ptr, builder, allocator, inheritance + 3, false, false);
			}
		}
		break;

		case Type_Kind::Primitive:
		{
			Primitive_Type* primitive_type = (Primitive_Type*) type;

			String str = primitive_to_string(primitive_type, mem, allocator);
			builder->append(str);
			allocator.free(str.data, code_location());
		}
		break;

		case Type_Kind::Enum:
		{
			Enum_Type* enum_type = (Enum_Type*) type;

			// @CopyPaste from write_thing_internal

			bool found_any_value = false;
			
			if (enum_type->flags)
			{
				bool had_value_before = false;

				for (Enum_Value enum_value: enum_type->values)
				{
					auto is_value_set = [](void* value, Primitive_Value to_check, u64 size) -> bool
					{
						u8* lhs = (u8*) value;
						u8* rhs = (u8*) &to_check;

						for (u64 i = 0; i < size; i++)
						{
							u8 a = *lhs;
							u8 b = *rhs;
							if ((a & b) != b) return false;

							lhs += 1;
							rhs += 1;
						}

						return true;
					};

					if (is_value_set(mem, enum_value.value, enum_type->size))
					{
						found_any_value = true;

						if (had_value_before)
						{
							builder->append(" | ");
						}

						had_value_before = true;

						builder->append(enum_value.name);
					}
				}
			}
			else
			{
				for (Enum_Value enum_value: enum_type->values)
				{
					if (memcmp(mem, &enum_value.value, enum_type->size) == 0)
					{
						builder->append(enum_value.name);
						found_any_value = true;
						break;
					}
				}
			}

			if (!found_any_value)
			{
				builder->append("No corresponding value was found."); // @TODO: maybe print number value
			}
		}
		break;

		case Type_Kind::String:
		{
			String_Type* string_type = (String_Type*) type;

			assert(string_type->string_kind == String_Kind::ASCII || string_type->string_kind == String_Kind::UTF32);

			builder->append("\"");

			if (string_type->string_kind == String_Kind::ASCII)
			{
				builder->append(*(String*) mem);
			}
			else
			{
				Unicode_String* str = (Unicode_String*) mem;
				int utf8_length;
				char* utf8 = str->to_utf8(allocator, &utf8_length);

				builder->append(String(utf8, utf8_length));

				allocator.free(utf8, code_location());
			}

			builder->append("\"");
		}
		break;

		default: assert(false);
	}
};


/*
	If we see pointer in struct or just a pointer to known type and it's not NULL
    we dereference it based on the 'dereference_pointers' boolean.
		But we do not dereference any embedded pointer of dereferenced struct.

	struct Str_0
	{
		Str_1* ptr_1;
	}

	struct Str_1
	{
		Str_0* ptr_0;
	}

	Str_0 str0;
	Str_1 str1;

	str0.ptr_1 = &str1;
	str1.ptr_0 = &str0;

	to_string(&str1);

	 In this case we will dereference the ptr_0 and print str0.
	  But we won't dereference any pointers inside str0, because it's embedded.
*/
inline String to_string(Reflection::Type* type, void* mem, Allocator allocator, bool dereference_pointers = true)
{
	String_Builder builder = build_string<char>(allocator);

	print_thing_to_builder(type, mem, &builder, allocator, 0, dereference_pointers);

	return builder.get_string();
}


template <typename T, std::enable_if_t<std::is_class_v<T> || std::is_enum_v<T> || std::is_pointer_v<T>, bool> = 0>
inline String to_string(T object, Allocator allocator, bool dereference_pointers = true)
{
	Reflection::Type* type = Reflection::type_of<T>();

	return to_string(type, &object, allocator, dereference_pointers);
}









// If number's base is not 10 than it's threated as unsigned.
template <typename T, std::enable_if_t<std::numeric_limits<T>::is_integer, bool> = 0>
inline bool parse_number(String str, T* result, int* result_base = NULL)
{
	static_assert(sizeof(T) == 1 || sizeof(T) == 2 || sizeof(T) == 4 || sizeof(T) == 8, "Integer size is not supported");

	constexpr bool is_signed = std::numeric_limits<T>::is_signed;

	if (str.length <= 0) return false;


	using Unsigned_T = typename std::make_unsigned<T>::type;

	union
	{
		T number = 0;
	};

	int base = 10;

	int start = 0;
	int end = str.length - 1;

	bool negative = false;

	if (str.length > 2 && str[start] == '0')
	{
		if (str[start + 1] == 'b' || str[start + 1] == 'B')
		{
			base = 2;
			start += 2;
		}
		else if (str[start + 1] == 'x' || str[start + 1] == 'X')
		{
			base = 16;
			start += 2;
		}
	}

	if (!is_signed || base != 10)
	{
		Unsigned_T number = 0;

		Unsigned_T limit;

#pragma push_macro("max")
		#undef max
		if (base == 10)
			limit = std::numeric_limits<T>::max();
		else
			limit = std::numeric_limits<Unsigned_T>::max();
#pragma pop_macro("max")



		Unsigned_T exponent_limit = limit / base;
		Unsigned_T exponent = 1;

		while (start <= end)
		{
			if (str[start] != '0')
				break;
			start += 1;
		}

		for (int i = end; i >= start; i--)
		{
			Unsigned_T digit;

			char c = str[i];

			if (c >= '0' && c <= '9')
				digit = c - '0';

			else if (c >= 'A' && c <= 'F')
				digit = c - 'A' + 10;

			else if (c >= 'a' && c <= 'a')
				digit = c - 'a' + 10;

			else
				return false;

			if (digit >= base) return false;

			if (digit != 0)
			{
				if (limit / digit < exponent) return false;

				Unsigned_T new_number = number + digit * exponent;

				if (new_number < number) return false;

				number = new_number;
			}

			bool exponent_overflow = exponent > exponent_limit;

			if (exponent_overflow)
			{
				if (i == start)
				{
					// We detected overflow, but it won't ruin anything
					// because we were going to leave the loop after this iteration anyway.
					break;
				}
				return false;
			}

			exponent *= base;
		}

		*((Unsigned_T*) result) = number;

		if (result_base)
			*result_base = base;

		return true;
	}

	if (str[0] == '-')
	{
		start += 1;
		negative = true;
	}

	while (start <= end)
	{
		if (str[start] != '0')
			break;

		start += 1;
	}


	assert(base == 10);

	T exponent = negative ? -1 : 1;

	// Because of how 2's complement numbers work (INT_MIN == -(INT_MAX + 1))
	// we can't just store magnitude of the number and just return magnitude * sign.

#pragma push_macro("max")
#pragma push_macro("min")
	#undef min
	#undef max
	T limit;
	if (negative)
		limit = std::numeric_limits<T>::min();
	else
		limit = std::numeric_limits<T>::max();
#pragma pop_macro("max")
#pragma pop_macro("min")


	T exponent_limit = limit / base;

	for (int i = end; i >= start; i--)
	{
		char c = str[i];

		T digit = c - '0';

		if (digit < 0 || digit > 9) return false;

		if (digit != 0)
		{
			if (negative)
			{
				if (limit / digit > exponent)
					return false;
			}
			else
			{
				if (limit / digit < exponent)
					return false;
			}
			// Android's bionic deals without 'limit / digit' division.
			// Instead they store 'cutlim' variable which in pair with exponent_limit can say if number
			// is in the valid range.
			//
			// https://android.googlesource.com/platform/bionic.git/+/eclair-release/libc/stdlib/strtol.c
		}

		T new_number = number + digit * exponent;

		if (negative)
		{
			if (new_number > number) return false;
		}
		else
		{
			if (new_number < number) return false;
		}

		number = new_number;

		bool exponent_overflow;
		if (negative)
			exponent_overflow = exponent < exponent_limit;
		else
			exponent_overflow = exponent > exponent_limit;


		if (exponent_overflow)
		{
			if (i == start)
			{
				// We detected overflow, but it won't ruin anything
				// because we were going to leave the loop after this iteration anyway.
				break;
			}

			// Overflow will happen if we multiply by base.
			return false;
		}

		exponent *= base;
	}

	*((T*) result) = number;

	if (result_base)
		*result_base = base;


	return true;
}


template <typename T, std::enable_if_t<std::is_same_v<T, float> || std::is_same_v<T, double>, bool> = 0>
inline bool parse_number(String str, T* result)
{
	static_assert(std::is_same_v<T, float> || std::is_same_v<T, double>);

	if (str.length <= 0) return false;

	if (compare_strings_case_independently(str, "-nan"))
	{
		*result = -NAN;
		return true;
	}
	else if (compare_strings_case_independently(str, "nan"))
	{
		*result = NAN;
		return true;
	}
	else if (compare_strings_case_independently(str, "-inf"))
	{
		*result = -INFINITY;
		return true;
	}
	else if (compare_strings_case_independently(str, "inf"))
	{
		*result = INFINITY;
		return true;
	}

	int position = 0;

	bool negative = false;

	if (str[0] == '-')
	{
		negative = true;
		position += 1;
	}

	int dot_position = -1;
	int e_position   = -1;

	while (position < str.length)
	{
		char c = str[position];

		if (c == '.')
		{
			if (dot_position == -1)
				dot_position = position;
			else
				return false;
		}
		else if (c == 'e' || c == 'E')
		{
			if (e_position == -1)
				e_position = position;
			else
				return false;
		}

		position += 1;
	}

	//
	// The code below uses integer version of parse_number which is bad,
	// because it's building garbage on top of garbage, but i just don't want
	// to write this routine properly enough, because it seems that it will be
	// complicated and dirty.
	//

	String fraction_string;

	if (dot_position == -1)
	{
		int start = negative ? 1 : 0;
		int length = (e_position == -1 ? str.length : e_position) - start;

		if (length > 18)
			length = 18;

		fraction_string = String(str.data + start, length);
	}
	else
	{
		char buffer[18]; // I read that digits that are after 18 won't affect at the value.

		int occupied = 0;

		int start = negative ? 1 : 0;
		int length = dot_position - start;
		if (length > 18)
			length = 18;

		occupied += length;

		memcpy(buffer, str.data + start, length);

		start = dot_position + 1;
		length = e_position == -1 ? (str.length - start) : (e_position - start);

		int remaining_space = 18 - occupied;
		if (length > remaining_space)
			length = remaining_space;

		memcpy(&buffer[occupied], str.data + start, length);

		occupied += length;

		fraction_string = String(buffer, occupied);
	}

	u64 fraction;

	int base;

	if (!parse_number(fraction_string, &fraction, &base)) return false;
	if (base != 10) return false;

	int exponent = 0;

	if (e_position != -1)
	{
		String exponent_string = String(str.data + e_position + 1, str.length - e_position - 1);

		// Exponent can have format like: "e00000013213232", zeros will be skipped by parse_number.
		// Could've made that error, but i'm too lazy.
		if (!parse_number(exponent_string, &exponent, &base)) return false;
		if (base != 10) return false;
	}

	if (dot_position != -1)
	{
		int dot_exponent = fraction_string.length - dot_position + (negative ? 1 : 0);
		exponent -= dot_exponent;
	}

	if (exponent < -300 || exponent > 300)
	{
		return false;
	}

	double value;

	value = (double) fraction;

	if (exponent != 0)
	{
		int abs_exponent = abs(exponent);

		if (abs_exponent >= 256)
		{
			if (exponent > 0) value *= 1e256;
			else              value /= 1e256;

			abs_exponent -= 256;
		}
		if (abs_exponent >= 128)
		{
			if (exponent > 0) value *= 1e128;
			else              value /= 1e128;

			abs_exponent -= 128;
		}
		if (abs_exponent >= 64)
		{
			if (exponent > 0) value *= 1e64;
			else              value /= 1e64;

			abs_exponent -= 64;
		}
		if (abs_exponent >= 32)
		{
			if (exponent > 0) value *= 1e32;
			else              value /= 1e32;

			abs_exponent -= 32;
		}
		if (abs_exponent >= 16)
		{
			if (exponent > 0) value *= 1e16;
			else              value /= 1e16;

			abs_exponent -= 16;
		}
		if (abs_exponent >= 8)
		{
			if (exponent > 0) value *= 1e8;
			else              value /= 1e8;

			abs_exponent -= 8;
		}
		if (abs_exponent >= 4)
		{
			if (exponent > 0) value *= 1e4;
			else              value /= 1e4;

			abs_exponent -= 4;
		}
		if (abs_exponent >= 2)
		{
			if (exponent > 0) value *= 1e2;
			else              value /= 1e2;

			abs_exponent -= 2;
		}
		if (abs_exponent >= 1)
		{
			if (exponent > 0) value *= 1e1;
			else              value /= 1e1;

			abs_exponent -= 1;
		}
	}

	if (negative)
		value = -value;

	*result = value;
	return true;
}

inline bool parse_primitive(String str, Reflection::Primitive_Type* type, void* mem)
{
	using namespace Reflection;

	switch (type->primitive_kind)
	{
		#define handle_number_primitive_type(type) case Primitive_Kind::P_##type:\
		{\
			type value;\
			bool result = parse_number(str, (type*) &value);\
			if (!result) return false;\
			memcpy(mem, &value, sizeof(type));\
			return true;\
		}\
		break;\

		handle_number_primitive_type(u8);
		handle_number_primitive_type(s8);
		handle_number_primitive_type(u16);
		handle_number_primitive_type(s16);
		handle_number_primitive_type(u32);
		handle_number_primitive_type(s32);
		handle_number_primitive_type(u64);
		handle_number_primitive_type(s64);

		handle_number_primitive_type(f32);
		handle_number_primitive_type(f64);

		case Primitive_Kind::P_bool:
		{
			if (str.length == 4)
			{
				if ((str[0] == 't' || str[0] == 'T') &&
					(str[1] == 'r' || str[1] == 'R') &&
					(str[2] == 'u' || str[2] == 'U') &&
					(str[3] == 'e' || str[3] == 'E'))
				{
					*(bool*) mem = true;
					return true;
				}
				return false;
			}
			else if (str.length == 5)
			{
				if ((str[0] == 'f' || str[0] == 'F') &&
					(str[1] == 'a' || str[1] == 'A') &&
					(str[2] == 'l' || str[2] == 'L') &&
					(str[3] == 's' || str[3] == 'S') && 
					(str[4] == 'e' || str[4] == 'E'))
				{
					*(bool*) mem = false;
					return true;
				}
				return false;
			}
			else
			{
				return false;
			}
		}
		break;

		default: assert(false);
	}

	return false;
}



inline void write_thing_internal(Reflection::Type* type, void* mem, String_Builder<char>* builder, int indentation)
{
	if (indentation > 8) return;

	Allocator allocator = builder->allocator;

	using namespace Reflection;

	auto append_indentation = [&](int indentation)
	{
		for (int i = 0; i < indentation; i++)
		{
			builder->append("\t");
		}
	};

	switch (type->kind)
	{
		case Type_Kind::Primitive:
		{
			String str = primitive_to_string((Primitive_Type*) type, mem, allocator);
			builder->append(str);
			allocator.free(str.data, code_location());
		}
		break;

		case Type_Kind::Enum:
		{
			Enum_Type* enum_type = (Enum_Type*) type;

			auto is_value_set = [](void* value, Primitive_Value to_check, u64 size) -> bool
			{
				u8* lhs = (u8*) &value;
				u8* rhs = (u8*) &to_check;

				for (u64 i = 0; i < size; i++)
				{
					if (*lhs & *rhs != *rhs) return false;

					lhs += 1;
					rhs += 1;
				}

				return true;
			};



			bool print_number_value_of_enum = false;


			if (enum_type->flags)
			{
				for (Enum_Value enum_value: enum_type->values)
				{
					if (is_value_set(mem, enum_value.value, enum_type->size))
					{
						builder->append(enum_value.name);
						builder->append(" | ");
					}
				}

				print_number_value_of_enum = true;
			}
			else
			{
				bool found_exactly_matching_value = false;

				for (Enum_Value enum_value: enum_type->values)
				{
					if (memcmp(mem, &enum_value.value, enum_type->size) == 0)
					{
						builder->append(enum_value.name);
						found_exactly_matching_value = true;
						break;
					}
				}

				print_number_value_of_enum = !found_exactly_matching_value;
			}


			if (print_number_value_of_enum)
			{
				// Here we print the number value of enum in base of 2.
				String str;
				switch (enum_type->size)
				{
					case 1:
						str = to_string<u8, 2>(*(u8*) mem, allocator);
						break;

					case 2:
						str = to_string<u16, 2>(*(u16*) mem, allocator);
						break;

					case 4:
						str = to_string<u32, 2>(*(u32*) mem, allocator);
						break;

					case 8:
						str = to_string<u64, 2>(*(u64*) mem, allocator);
						break;

					default: 
						assert(false);
				}
				builder->append(str);
				allocator.free(str.data, code_location());
			}
		}
		break;


		case Type_Kind::String:
		{
			String_Type* string_type = (String_Type*) type;

			assert(string_type->string_kind == String_Kind::ASCII || string_type->string_kind == String_Kind::UTF32);

			char* utf8;
			int utf8_length;


			if (string_type->string_kind == String_Kind::ASCII)
			{
				String* str = (String*) mem;
				utf8 = str->data;
				utf8_length = str->length;
			}
			else
			{
				Unicode_String* str = (Unicode_String*) mem;
				utf8 = str->to_utf8(allocator, &utf8_length);
			}

			builder->append("\"");
			{
				for (int i = 0; i < utf8_length; i++)
				{
					char c = utf8[i];
					if (c == '\\')
					{
						builder->append("\\\\");
					}
					else if (c == '\"')
					{
						builder->append("\\\"");
					}
					else
					{
						builder->append(c);
					}
				}
			}
			builder->append("\"");


			if (string_type->string_kind == String_Kind::UTF32)
			{
				allocator.free(utf8, code_location());
			}
		}
		break;



		case Type_Kind::Struct:
		{
			Struct_Type* struct_type = (Struct_Type*) type;

			builder->append("{\n");

			for (auto iter = struct_type->iterate_members(allocator); Struct_Member* member = iter.next();)
			{
				if (member->type->kind == Type_Kind::Pointer)
				{
					assert(false); // @TODO: probably you should specify MEMBER_DO_NOT_SERIALIZE flag (This flag is not implemented yet.)
					// :WritingPointers
					continue;
				}

				append_indentation(indentation + 1);

				builder->append(member->name);
				builder->append(":");

				write_thing_internal(member->type, add_bytes_to_pointer(mem, member->offset), builder, indentation + 1);

				builder->append("\n");
			}

			append_indentation(indentation);
			builder->append("}");
		}
		break;


		case Type_Kind::Array:
		{
			Array_Type* array_type = (Array_Type*) type;

			builder->append("[\n");

			Dynamic_Array<int>* array = (Dynamic_Array<int>*) mem;

			int item_size = array_type->inner_type->size;

			for (int i = 0; i < array->count; i++)
			{
				append_indentation(indentation + 1);
				write_thing_internal(array_type->inner_type, add_bytes_to_pointer(array->data, i * item_size), builder, indentation + 1);

				if (i != array->count - 1)
				{
					builder->append(",");
				}
				builder->append("\n");
			}

			append_indentation(indentation);
			builder->append("]");
		}
		break;

		case Type_Kind::Pointer:
		{
			//:WritingPointers Do nothing with pointers
			assert(false);
		}
		break;

		default: assert(false);
	}
}




inline void write_thing_to_builder(String_Builder<char>* builder, Reflection::Type* type, void* mem)
{
	write_thing_internal(type, mem, builder, 0);
}

template <typename T>
inline void write_thing_to_builder(String_Builder<char>* builder, T thing)
{
	write_thing_to_builder(builder, Reflection::type_of<T>(), &thing);
}

inline String write_thing(Reflection::Type* type, void* mem, Allocator allocator)
{
	using namespace Reflection;

	String_Builder<char> builder = build_string<char>(allocator);

	write_thing_to_builder(&builder, type, mem);

	return builder.get_string();
}

template <typename T>
inline String write_thing(T thing, Allocator allocator)
{
	return write_thing(Reflection::type_of<T>(), &thing, allocator);
}


