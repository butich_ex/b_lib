#pragma once

#include "Allocator.h"
#include "String.h"

template <typename Char_Type>
struct String_Builder
{
	Char_Type* buffer;
	int length;
	int capacity;

	Allocator allocator;

	String_Builder() {};

	inline void ensure_buffer_size(int target_length)
	{
		if (target_length <= capacity)
		{
			return;
		}

		size_t old_capacity = capacity;
		capacity = max(target_length, capacity + 256);

		buffer = (Char_Type*) allocator.realloc(buffer, old_capacity * sizeof(Char_Type), capacity * sizeof(Char_Type), code_location());
	}

	inline void append(int index, Template_String<Char_Type> str)
	{
		ensure_buffer_size(str.length + length);

		if (index < length)
		{
			memcpy(buffer + index + str.length, buffer + index, (length - index) * sizeof(Char_Type));

			assert(index + str.length + (length - index) <= capacity);
		}

		memcpy(buffer + index, str.data, str.length * sizeof(Char_Type));

		length += str.length;
	}
	inline void append(Template_String<Char_Type> str)
	{
		append(length, str);
	};
	inline void append(int index, Char_Type c)
	{
		append(index, Template_String<Char_Type>(&c, 1));
	}
	inline void append(Char_Type c)
	{
		append(length, c);
	}

	inline void remove(int index, int length)
	{
		assert(index <= this->length);
		assert(length <= this->length - index);

		memcpy(buffer + index, buffer + index + length, (this->length - index - length) * sizeof(Char_Type));

		this->length -= length;
	}

	inline void clear()
	{
		length = 0;
	}

	inline void free()
	{
		allocator.free(buffer, code_location());
		buffer = NULL;
	}

 	// Don't forget to free the result string!!!! String.data points to builder.buffer.
	inline Template_String<Char_Type> get_string()
	{
		return Template_String<Char_Type>(buffer, length);
	}
	// This will return the String allocated in new place.
	inline Template_String<Char_Type> get_copied_string()
	{
		return get_string().copy_with(allocator);
	}
};

template <typename Char_Type>
inline String_Builder<Char_Type> build_string(Allocator allocator, size_t initial_buffer_capacity = 256)
{
	String_Builder<Char_Type> builder;

	builder.allocator = allocator;

	builder.buffer = (Char_Type*) allocator.alloc(initial_buffer_capacity * sizeof(Char_Type), code_location());

	builder.length = 0;
	builder.capacity = initial_buffer_capacity;

	return builder;
}
