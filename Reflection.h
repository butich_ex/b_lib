#pragma once

#include "Basic.h"
#include "String.h"
#include "Dynamic_Array.h"
#include "Allocator.h"
#include <type_traits>
#include <cstddef>
#include "String_Builder.h"


/* Usage:

struct Some_Struct
{
	int   a;
	float b;
	void* c;
}
REFLECT(SOME_STRUCT)
	MEMBER(a);
	MEMBER(b);
	MEMBER(c);
REFLECT_END();


enum class Some_Enum: u32
{
	First,
	Second = 3,
}
REFLECT(SOME_ENUM)
	ENUM_VALUE(First);
	ENUM_VALUE(Second);
REFLECT_END();


Uses c_allocator for memory_allocation.
You don't have to call register yourself. It's done statically.


Type* type = Reflection::type_of<T>();
And then check check type->kind to know specific.

If you ask for type that hasn't been registered yet, there are 2 cases.
	1) If you pointer that ends up to the registered type,
	   	we add it to types and return.
	2) In other case we assert(false).
*/



namespace Reflection
{
	struct Type;
};


void print_types_that_referenced_this_type(Reflection::Type* type);



namespace Reflection
{
	const Allocator R_allocator = c_allocator;

	inline bool reflection_initialized = false;
	inline bool allow_runtime_type_generation = false;

	struct Type;

	enum class Type_Kind: u32
	{
		Undefined = 0,

		Struct,
		Primitive,
		Pointer,
		Enum,
		Array,
		String,

		Substitution, // This is used for type generation. Not used by user-level application.
	};


	struct Type
	{
		String name;
		Type_Kind kind;
		u32 size;
		u32 id;
		u32 user_flags = 0;
	};

	struct Primitive_Value
	{
		union
		{
			u8  u8_value;
			u16 u16_value;
			u32 u32_value;
			u64 u64_value;
			s8  s8_value;
			s16 s16_value;
			s32 s32_value;
			s64 s64_value;

			f32 f32_value;
			f64 f64_value;

			bool bool_value;

			// @NewPrimitive
		};
	};

	// u32 value represent index in type table. So make sure that primitives use first slots.
	enum class Primitive_Kind: u32
	{
		P_u8  = 0,
		P_s8  = 1,
		P_u16 = 2,
		P_s16 = 3,
		P_u32 = 4,
		P_s32 = 5,
		P_u64 = 6,
		P_s64 = 7,

		P_f32 = 8,
		P_f64 = 9,

		P_bool = 10,
		P_void = 11,

		MAX_PRIMITIVE_COUNT = 12,

		// @NewPrimitive
	};

	struct Primitive_Type: Type
	{
		Primitive_Kind primitive_kind;
	};

	struct Pointer_Type: Type
	{
		Type* inner_type;
	};

	struct Enum_Value
	{
		String  name;
		Primitive_Value value;
	};

	struct Enum_Type: Type
	{
		Primitive_Type* base_type;

		bool flags = false;

		Dynamic_Array<Enum_Value> values;
	};

	struct Array_Type: Type
	{
		Type* inner_type;
	};


	enum class String_Kind: u32
	{
		ASCII = 0,
		UTF32 = 1,

		MAX_STRING_KIND_COUNT = 2,
	};
	struct String_Type: Type
	{
		String_Kind string_kind;
	};


	struct Struct_Member
	{
		String name;
		Type* type;
		u32 offset;
	};

	struct Struct_Type: Type
	{
		Struct_Type* base_type;

		Dynamic_Array<Struct_Member> members;

		struct Members_Iterator
		{
			Struct_Type* type;
			int index;

			Members_Iterator* base_type_members_iter;

			Allocator allocator;

			Struct_Member* next();
		};

		// This will recursively iterate through members of base_type plus members of this struct.
		Members_Iterator iterate_members(Allocator allocator);
	};


	constexpr size_t type_struct_max_size()
	{
		return max(max(max(max(max(sizeof(Primitive_Type), sizeof(Pointer_Type)), sizeof(Enum_Type)), sizeof(Struct_Type)), sizeof(Array_Type)), sizeof(String_Type));
	}

	struct Max_Type_Size_Struct
	{
		char placeholder[type_struct_max_size()];
	};

	inline Dynamic_Array<Max_Type_Size_Struct> types;


	// first N id's occupied by primitives and string types of different kind.
	inline u32 R_unique_type_id = ((u32) Primitive_Kind::MAX_PRIMITIVE_COUNT + (u32) String_Kind::MAX_STRING_KIND_COUNT);
	inline u32 get_unique_type_id()
	{
		defer { R_unique_type_id += 1; };
		return R_unique_type_id;
	}

	template <typename T>
	struct Type_Id
	{
		inline static u32 value = 0; // This struct's instances will endup in .bss. so this will be 0.

		inline static u32 get_id()
		{
			// This code shouldn't be used for primitives !!

			if (value == 0)
			{
				value = get_unique_type_id();
			}
			return value;
		}
	};

	template <>
	struct Type_Id<u8>
	{
		const u32 value = (u32) Primitive_Kind::P_u8;
	};
	template <>
	struct Type_Id<s8>
	{
		const u32 value = (u32) Primitive_Kind::P_s8;
	};
	template <>
	struct Type_Id<signed char>
	{
		const u32 value = (u32) Primitive_Kind::P_s8;
	};
	template <>
	struct Type_Id<u16>
	{
		const u32 value = (u32) Primitive_Kind::P_u16;
	};
	template <>
	struct Type_Id<char16_t>
	{
		const u32 value = (u32) Primitive_Kind::P_u16;
	};
	template <>
	struct Type_Id<s16>
	{
		const u32 value = (u32) Primitive_Kind::P_s16;
	};
	template <>
	struct Type_Id<u32>
	{
		const u32 value = (u32) Primitive_Kind::P_u32;
	};
	template <>
	struct Type_Id<char32_t>
	{
		const u32 value = (u32) Primitive_Kind::P_u32;
	};
	template <>
	struct Type_Id<s32>
	{
		const u32 value = (u32) Primitive_Kind::P_s32;
	};
	template <>
	struct Type_Id<u64>
	{
		const u32 value = (u32) Primitive_Kind::P_u64;
	};
	template <>
	struct Type_Id<s64>
	{
		const u32 value = (u32) Primitive_Kind::P_s64;
	};
	template <>
	struct Type_Id<f32>
	{
		const u32 value = (u32) Primitive_Kind::P_f32;
	};
	template <>
	struct Type_Id<f64>
	{
		const u32 value = (u32) Primitive_Kind::P_f64;
	};
	template <>
	struct Type_Id<String>
	{
		const u32 value = (u32) Primitive_Kind::MAX_PRIMITIVE_COUNT + (u32) String_Kind::ASCII;
	};
	template <>
	struct Type_Id<Unicode_String>
	{
		const u32 value = (u32) Primitive_Kind::MAX_PRIMITIVE_COUNT + (u32) String_Kind::UTF32;
	};






	template <typename T>
	inline Type* infer_type();


	struct Types_Iterator
	{
		int index = 0;

		Type* next()
		{
			if (index >= types.count) return NULL;

			defer{ index += 1; };

			return (Type*) types[index];
		}
	};
	inline Types_Iterator iterate_types()
	{
		assert(reflection_initialized && "Call Reflection::init() before!!");
		return Types_Iterator();
	}


	template <typename T>
	inline Type* type_of()
	{
		assert_msg(reflection_initialized, "Call Reflection::init() before!!");

		u32 id = Type_Id<T>::get_id();

		Type* type = (Type*) types[id];

		if (id == 0 || type->kind == Type_Kind::Substitution || type->kind == Type_Kind::Undefined)
		{
			if (allow_runtime_type_generation)
			{
				Type* inferred_type = infer_type<T>();
				assert(type == inferred_type);

				return inferred_type;
			}
			else
			{
				abort_the_mission<>(U"Type you requesting has not been registered");
				return NULL;	
			}
		}

		return type;
	}


// THis is useless, because we to to_string(enum)
#if 0
	template <typename T>
	inline bool get_enum_value_name(T value, String* out_result)
	{
		assert_msg(reflection_initialized, "Call Reflection::init() before!!");

		Enum_Type* type = (Enum_Type*) type_of<T>();
		assert(type->type_kind == Type_Kind::Enum);

		for (Enum_Value enum_value: type->values)
		{
			if (memcmp(&value, &enum_value.value, sizeof(value)) == 0)
			{
				*out_result = enum_value.name;
				return true;
			}
		}
	
		return false;
	}
#endif

	template <typename T>
	inline bool get_enum_value(String str, Primitive_Value* out_value)
	{
		assert_msg(reflection_initialized, "Call Reflection::init() before!!");

		Enum_Type* type = (Enum_Type*) type_of<T>();
		assert(type->kind == Type_Kind::Enum);

		for (Enum_Value enum_value: type->values)
		{
			if (enum_value.name == str)
			{
				*out_value = enum_value.value;
				return true;
			}
		}
	
		return false;
	}




	inline Type* get_pointer_inner_type_with_indirection_level(Pointer_Type* pointer_type, int* result_indirection_level)
	{
		int indirection_level = 1;

		while (pointer_type->inner_type->kind == Type_Kind::Pointer)
		{
			indirection_level += 1;
			pointer_type = (Pointer_Type*) pointer_type->inner_type;
		}

		*result_indirection_level = indirection_level;

		return pointer_type->inner_type;
	}




	template <typename T, bool = std::is_enum<T>::value>
	struct relaxed_underlying_type {
		using type = typename std::underlying_type<T>::type;
	};
	template <typename T>
	struct relaxed_underlying_type<T, false> {
		using type = T;
	};

	#define REGISTER_TYPE(type_name) type_name :: CONCAT(reflection_register_, type_name)();

	#ifdef TRACY_ENABLE
		#define REFLECT_TRACY_ZONE_SCOPED ZoneScoped
	#else
		#define REFLECT_TRACY_ZONE_SCOPED 
	#endif


	#define REFLECT(type_name)\
	inline static void CONCAT(reflection_register_, type_name ) ();\
	inline Registrar CONCAT(reflection_register_struct_, type_name)(CONCAT(reflection_register_, type_name ));\
	inline static void CONCAT(reflection_register_, type_name ) () {\
		REFLECT_TRACY_ZONE_SCOPED ;\
		using R_T = type_name;\
		R_T instance = R_T();\
		Reflection::Type* enum_value_type;\
		union {\
			Reflection::Type* type;\
			Reflection::Struct_Type* struct_type;\
			Reflection::Enum_Type*   enum_type;\
		};\
		type = Reflection::create_type_or_get_substitution(Reflection::Type_Id<R_T>::get_id());\
		type->size = sizeof(R_T);\
		type->name = String( #type_name );\
		if constexpr (std::is_class<R_T>::value) {\
			type->kind = Reflection::Type_Kind::Struct;\
			struct_type->base_type = NULL;\
			struct_type->members = Dynamic_Array<Reflection::Struct_Member>(8, Reflection::R_allocator);\
		}\
		else if constexpr (std::is_enum<R_T>::value) {\
			using Enum_Base_T = Reflection::relaxed_underlying_type<R_T>::type;\
			type->kind = Reflection::Type_Kind::Enum;\
			enum_value_type = Reflection::infer_type<Enum_Base_T>();\
			assert(enum_value_type->kind == Reflection::Type_Kind::Primitive);\
			enum_type->base_type = (Reflection::Primitive_Type*) enum_value_type;\
			enum_type->values = Dynamic_Array<Reflection::Enum_Value>(8, Reflection::R_allocator);\
			enum_type->flags = false;\
		}\
		assert(type->kind == Reflection::Type_Kind::Struct || type->kind == Reflection::Type_Kind::Enum);\
		bool declared_base_type_already = false;

	#define USER_FLAGS( flags ) type->user_flags = flags;

	#define BASE_TYPE( type_name )\
		{\
			assert_msg(!struct_type->base_type, "Reflection doesn't support multiple inheritance");\
			static_assert(std::is_base_of_v< type_name , R_T >, "You passed wrong base_type BASE_TYPE is wrong");\
			struct_type->base_type = (Struct_Type*) infer_type< type_name >();\
		};


	#define MEMBER(member_name)\
		{\
			Reflection::Struct_Member struct_member;\
			struct_member.name = String( #member_name );\
			using Member_T = decltype(R_T :: member_name );\
			struct_member.type = Reflection::infer_type<Member_T>();\
			struct_member.offset = offsetof(R_T, member_name );\
			struct_type->members.add(struct_member);\
		};

	#define ENUM_VALUE(member_name)\
		{\
			Reflection::Enum_Value enum_value;\
			enum_value.name = String( #member_name );\
			union\
			{\
				Reflection::Primitive_Value primitive_value;\
				R_T enum_typed_value;\
			};\
			enum_typed_value = R_T :: member_name ;\
			enum_value.value = primitive_value;\
			enum_type->values.add(enum_value);\
		};

	#define MEMBER_NAME(member_name)\
		{\
			assert(struct_type->members.count > 0);\
			Reflection::Struct_Member* member = struct_type->members.last();\
			member->name = member_name;\
		};

	#define ENUM_FLAGS(value) enum_type->flags = value;

	#define REFLECT_END() };


	inline void register_primitive_and_string_types();


	inline Type* get_type_at_id(u32 type_id)
	{
		// After Reflection::init is called type table must be full and we're not allowed to change Type* pointers anymore
		//  that guarantees that pointers still the same
		assert(!reflection_initialized || allow_runtime_type_generation);

		// We have to make every type slot's id = 0 to mark that it's not used yet.

		if (types.data == NULL)
		{
			types = Dynamic_Array<Max_Type_Size_Struct>(64, c_allocator);
			types.count = (u32) Primitive_Kind::MAX_PRIMITIVE_COUNT + (u32) String_Kind::MAX_STRING_KIND_COUNT;

			for (int i = 0; i < types.capacity; i++)
			{
				Type* type = (Type*) types[i];
				*type = Type();
				type->id = 0;
			}

			register_primitive_and_string_types();
		}

		{
			int previous_capacity = types.capacity;
			void* previous_data = (void*) types.data;

			types.ensure_capacity(type_id + 1);

			if (previous_capacity != types.capacity)
			{
				// Make non occupied slots marked as non used.
				for (int i = previous_capacity; i < types.capacity; i++)
				{
					Type* type = (Type*) types[i];
					*type = Type();
					type->id = 0;
				}


				// Shift inner type pointers
				u64 inner_pointer_offset = (u64) types.data - (u64) previous_data;

				// Doesn't really matter if pointers are valid just offset all of them.
				for (int i = 0; i < types.capacity; i++)
				{
					Type* type = (Type*) types[i];
					if (type->id == 0) continue;

					switch (type->kind)
					{
						case Type_Kind::Struct:
						{
							Struct_Type* struct_type = (Struct_Type*) type;

							struct_type->base_type = add_bytes_to_pointer(struct_type->base_type, inner_pointer_offset);

							for (Struct_Member& member: struct_type->members)
							{
								member.type = add_bytes_to_pointer(member.type, inner_pointer_offset);
							}
						}
						break;

						case Type_Kind::Pointer:
						{
							Pointer_Type* pointer_type = (Pointer_Type*) type;
							pointer_type->inner_type = add_bytes_to_pointer(pointer_type->inner_type, inner_pointer_offset);
						}
						break;

						case Type_Kind::Enum:
						{
							Enum_Type* enum_type = (Enum_Type*) type;
							enum_type->base_type = add_bytes_to_pointer(enum_type->base_type, inner_pointer_offset);
						};
					}
				}
			}
		}

		types.count = max<int>(types.count, type_id + 1);

		Type* type = (Type*) types[type_id];

		return type;
	}

	inline Primitive_Type* get_primitive_type(Primitive_Kind primitive_kind)
	{
		assert(primitive_kind >= Primitive_Kind::P_u8 && primitive_kind < Primitive_Kind::MAX_PRIMITIVE_COUNT);

		//assert(types.count >= (u32) Primitive_Kind::MAX_PRIMITIVE_COUNT);

		return (Primitive_Type*) types[(u32) primitive_kind];
	}

	inline String_Type* get_string_type(String_Kind string_kind)
	{
		// String types packed after primitives.

		assert(string_kind == String_Kind::ASCII || string_kind == String_Kind::UTF32);

		//assert(types.count >= (u32) Primitive_Kind::MAX_PRIMITIVE_COUNT + (u32) String_Kind::MAX_STRING_KIND_COUNT);

		return (String_Type*) types[(u32) Primitive_Kind::MAX_PRIMITIVE_COUNT + (u32) string_kind];
	}


	inline Type* get_type_or_create_substitution(u32 type_id)
	{
		Type* type = get_type_at_id(type_id);
		
		if (type->id == 0)
		{
			type->id = type_id;
			type->kind = Type_Kind::Substitution;
		}

		return type;
	}

	inline Type* create_type_or_get_substitution(u32 type_id)
	{
		Type* type = get_type_at_id(type_id);
		
		if (type->id == 0)
		{
			type->id = type_id;
		}

		return type;
	}


	template <typename T>
	inline Type* infer_pointer_type()
	{
		static_assert(std::is_pointer<T>::value);

		using Child_Type = typename std::remove_pointer<T>::type;

		Type* inner_type = infer_type<Child_Type>();

		u32 type_id = Type_Id<T>::get_id();

		Pointer_Type* pointer_type = (Pointer_Type*) get_type_at_id(type_id);
		if (pointer_type->kind == Type_Kind::Pointer)
		{
			return pointer_type;
		}

		pointer_type->name = String::empty;
		pointer_type->kind = Type_Kind::Pointer;
		pointer_type->size = sizeof(void*);

		pointer_type->inner_type = inner_type;

		return pointer_type;
	}

	template <typename T>
	inline Type* infer_array_type()
	{
		static_assert(is_template_instance<T, Dynamic_Array>::value);

		using Child_Type = typename extract_value_type<T>::value_type;

		Type* inner_type = infer_type<Child_Type>();

		u32 type_id = Type_Id<T>::get_id();

		Array_Type* array_type = (Array_Type*) get_type_at_id(type_id);
		if (array_type->kind == Type_Kind::Array)
		{
			return array_type;
		}

		array_type->name = String::empty;
		array_type->kind = Type_Kind::Array;
		array_type->size = sizeof(Dynamic_Array<int>);

		array_type->inner_type = inner_type;

		return array_type;
	}



	template <typename T>
	inline Type* infer_type()
	{
		if (reflection_initialized)
		{
			assert(allow_runtime_type_generation);
		}


		if      constexpr (std::is_same_v<T, u8>)   return get_primitive_type(Primitive_Kind::P_u8);
		else if constexpr (std::is_same_v<T, s8> || std::is_same_v<T, signed char>)   return get_primitive_type(Primitive_Kind::P_s8);
		else if constexpr (std::is_same_v<T, u16> || std::is_same_v<T, char16_t>)  return get_primitive_type(Primitive_Kind::P_u16);
		else if constexpr (std::is_same_v<T, s16>)  return get_primitive_type(Primitive_Kind::P_s16);
		else if constexpr (std::is_same_v<T, u32> || std::is_same_v<T, char32_t>)  return get_primitive_type(Primitive_Kind::P_u32);
		else if constexpr (std::is_same_v<T, s32>)  return get_primitive_type(Primitive_Kind::P_s32);
		else if constexpr (std::is_same_v<T, u64>)  return get_primitive_type(Primitive_Kind::P_u64);
		else if constexpr (std::is_same_v<T, s64>)  return get_primitive_type(Primitive_Kind::P_s64);

		else if constexpr (std::is_same_v<T, f32>)  return get_primitive_type(Primitive_Kind::P_f32);
		else if constexpr (std::is_same_v<T, f64>)  return get_primitive_type(Primitive_Kind::P_f64);

		else if constexpr (std::is_same_v<T, bool>) return get_primitive_type(Primitive_Kind::P_bool);
		else if constexpr (std::is_same_v<T, void>) return get_primitive_type(Primitive_Kind::P_void);

		else if constexpr (std::is_same_v<T, String>)          return get_string_type(String_Kind::ASCII);
		else if constexpr (std::is_same_v<T, Unicode_String>)  return get_string_type(String_Kind::UTF32);


		// @NewPrimitive



		else if constexpr (std::is_pointer<T>::value)
		{
			return infer_pointer_type<T>();
		}
		else if constexpr(is_template_instance<T, Dynamic_Array>::value)
		{
			return infer_array_type<T>();
		}
		else
		{
			u32 type_id = Type_Id<T>::get_id();
			assert(type_id != 0);
			return get_type_or_create_substitution(type_id);
		}
	}


	inline u32 primitive_kind_size(Primitive_Kind kind)
	{
		switch (kind)
		{
			case Primitive_Kind::P_u8:  return  8 / 8;
			case Primitive_Kind::P_s8:  return  8 / 8;
			case Primitive_Kind::P_u16: return 16 / 8;
			case Primitive_Kind::P_s16: return 16 / 8;
			case Primitive_Kind::P_u32: return 32 / 8;
			case Primitive_Kind::P_s32: return 32 / 8;
			case Primitive_Kind::P_u64: return 64 / 8;
			case Primitive_Kind::P_s64: return 64 / 8;

			case Primitive_Kind::P_f32: return 32 / 8;
			case Primitive_Kind::P_f64: return 64 / 8;

			case Primitive_Kind::P_bool: return sizeof(bool);

			case Primitive_Kind::P_void: return 0;

			// @NewPrimitive

			default: assert(false);
		}
		return 0;
	}



	inline String primitive_kind_name(Primitive_Kind kind)
	{
		switch (kind)
		{
			case Primitive_Kind::P_u8:  return "u8";
			case Primitive_Kind::P_s8:  return "s8";
			case Primitive_Kind::P_u16: return "u16";
			case Primitive_Kind::P_s16: return "s16";
			case Primitive_Kind::P_u32: return "u32";
			case Primitive_Kind::P_s32: return "s32";
			case Primitive_Kind::P_u64: return "u64";
			case Primitive_Kind::P_s64: return "s64";

			case Primitive_Kind::P_f32: return "f32";
			case Primitive_Kind::P_f64: return "f64";

			case Primitive_Kind::P_bool: return "bool";

			case Primitive_Kind::P_void: return "void";

			// @NewPrimitive


			default: assert(false);
		}
		return String::empty;
	}


	inline void register_primitive(Primitive_Kind kind)
	{
		u32 type_id = (u32) kind;

		Primitive_Type* type = (Primitive_Type*) get_type_at_id(type_id);

		type->kind = Type_Kind::Primitive;

		type->primitive_kind = kind;
		type->name = primitive_kind_name(kind);
		type->size = primitive_kind_size(kind);

	}

	inline void register_primitive_and_string_types()
	{
		register_primitive(Primitive_Kind::P_u8);
		register_primitive(Primitive_Kind::P_s8);

		register_primitive(Primitive_Kind::P_s16);
		register_primitive(Primitive_Kind::P_u16);

		register_primitive(Primitive_Kind::P_s32);
		register_primitive(Primitive_Kind::P_u32);

		register_primitive(Primitive_Kind::P_s64);
		register_primitive(Primitive_Kind::P_u64);

		register_primitive(Primitive_Kind::P_f32);
		register_primitive(Primitive_Kind::P_f64);

		register_primitive(Primitive_Kind::P_bool);

		register_primitive(Primitive_Kind::P_void);

		// @NewPrimitive

		// ASCII string
		{
			u32 type_id = (u32) Primitive_Kind::MAX_PRIMITIVE_COUNT + (u32) String_Kind::ASCII;

			String_Type* type = (String_Type*) get_type_at_id(type_id);

			type->kind = Type_Kind::String;

			type->string_kind = String_Kind::ASCII;
			type->name = "String";
			type->size = sizeof(String);
		}

		// UTF-32 string
		{
			u32 type_id = (u32) Primitive_Kind::MAX_PRIMITIVE_COUNT + (u32) String_Kind::UTF32;

			String_Type* type = (String_Type*) get_type_at_id(type_id);

			type->kind = Type_Kind::String;

			type->string_kind = String_Kind::UTF32;
			type->name = "Unicode_String";
			type->size = sizeof(Unicode_String);
		}
	}

	inline void init()
	{
		reflection_initialized = true;


		for (auto iter = iterate_types(); Type* type = iter.next();)
		{
			if (type->kind == Type_Kind::Substitution)
			{
				print_types_that_referenced_this_type(type);

				assert_msg(false, "Substitution left.");
			}
		}

		for (auto iter = iterate_types(); Type* type = iter.next();)
		{
			if (type->kind == Type_Kind::Undefined)
			{
				print_types_that_referenced_this_type(type);
				
				assert_msg(false, "Found an undefined type");
			}
			if (type->kind == Type_Kind::Struct)
			{
				Struct_Type* struct_type = (Struct_Type*) type;
				if (struct_type->base_type)
				{
					assert_msg(struct_type->base_type->kind == Type_Kind::Struct, "Structs are only allowed to inherit from other structs");
				}
			}
		}
	}


	inline Struct_Type::Members_Iterator Struct_Type::iterate_members(Allocator allocator)
	{
		Members_Iterator iter;
		iter.type = this;
		iter.index = -1;
		iter.allocator = allocator;
		iter.base_type_members_iter = NULL;

		return iter;
	}

	inline Struct_Member* Struct_Type::Members_Iterator::next()
	{
		while (true)
		{
			if (base_type_members_iter)
			{
				Struct_Member* inner_result = base_type_members_iter->next();

				if (!inner_result)
				{
					allocator.free(base_type_members_iter, code_location());

					base_type_members_iter = NULL;
					index = 0;
				}

				return inner_result;
			}
			else if (index == -1)
			{
				if (type->base_type)
				{
					base_type_members_iter = allocate_and_copy(&type->base_type->iterate_members(allocator), allocator);
					continue;
				}
				else
				{
					index = 0;
				}
			}

			if (index == type->members.count) return NULL;

			defer { index += 1; };
			return type->members[index];
		}
	}
}
