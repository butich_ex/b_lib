#pragma once
#include "Basic.h"
#include "String.h"
#include "Format_String.h"
#include "Log.h"
#include "Context.h"

#include <initializer_list>



#if OS_WINDOWS
#include <windows.h>
#include <io.h>
#elif OS_LINUX
#include <stdio.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#endif



const char path_separator = '/';

inline bool is_path_separator(char32_t c)
{
	return c == U'/' || c == U'\\';
}



struct Files_Iterator
{
	Allocator allocator;

#if OS_WINDOWS
	HANDLE search_handle;
	WIN32_FIND_DATAW find_data;
#elif OS_LINUX
	char* utf8_dir;
	DIR* dir;
#else
	static_assert(false);
#endif


	Files_Iterator() {};


	Unicode_String current = Unicode_String::empty;

	Unicode_String next()
	{
	#if OS_WINDOWS

		while (true)
		{
			if (FindNextFileW(search_handle, &find_data))
			{
   				if (find_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) continue;

				// @MemoryLeak
				current = Unicode_String::from_utf16((char16_t*) &find_data.cFileName, allocator);

				// '.' is already should be skipped
				if (current == U"..") continue;

				break;
			}

			current = Unicode_String::empty;

			break;
		}

		return current;

	#elif OS_LINUX
		while (true)
        {
		    dirent* dp = readdir(dir);
		    if (!dp)
            {
		        current = Unicode_String::empty;
		        break;
            }

		    if (dp->d_type != DT_REG) continue;

			// @MemoryLeak
		    current = Unicode_String::from_utf8((char*) dp->d_name, allocator);
		    break;
        }

        return current;

	#else
		static_assert(false);
	#endif
	}

	bool succeeded_to_open()
	{
	#if OS_WINDOWS
		return search_handle != INVALID_HANDLE_VALUE;
	#elif OS_LINUX
		return dir != NULL;
	#endif
	}

	void close()
	{
		if (succeeded_to_open())
		{
	#if OS_WINDOWS
			FindClose(search_handle);
			search_handle = INVALID_HANDLE_VALUE;

	#elif OS_LINUX
			closedir(dir);
			dir = NULL;
	#endif
		}
	}
};

inline Files_Iterator iterate_files(Unicode_String directory, Allocator allocator)
{
	Files_Iterator iter;

	iter.allocator = allocator;

#if OS_WINDOWS

	Unicode_String search_string;
	if (directory.length > 0)
	{
		if (is_path_separator(directory[directory.length - 1]))
		{
			search_string = format_unicode_string(allocator, U"%*\0", directory);
		}
		else
		{
			search_string = format_unicode_string(allocator, U"%/*\0", directory);
		}
	}

	int utf16_length;
	char16_t* utf16str = search_string.to_utf16(allocator, &utf16_length); // @MemoryLeak

	allocator.free(search_string.data, code_location());

	iter.search_handle = FindFirstFileW((wchar_t*) utf16str, &iter.find_data);
	if (!iter.succeeded_to_open())
	{
		return iter;
	}

	iter.current = Unicode_String::from_utf16((char16_t*) &iter.find_data.cFileName, allocator); // @MemoryLeak

	assert(iter.current == U".");

	return iter;

#elif OS_LINUX

	int utf8_length;
	iter.utf8_dir = directory.to_utf8(allocator, &utf8_length);
	iter.dir = opendir(iter.utf8_dir);

	return iter;

#else
	static_assert(false);
#endif
}


template <typename Char_Type>
inline Template_String<Char_Type> get_file_name_without_extension(Template_String<Char_Type> file_path)
{
	int dot_index = file_path.length; //If we didn't found dot we'll be fine
	int separator_index = -1;

	for (int i = file_path.length - 1; i >=0; i--)
	{
		Char_Type c = file_path.data[i];

		if (c == '.')
		{
			dot_index = i;
		}
		if (is_path_separator(c))
		{
			separator_index = i;
			break;
		}
	}

	assert(separator_index < dot_index);

	return file_path.sliced(separator_index + 1, dot_index - separator_index - 1);
}

template <typename Char_Type>
Template_String<Char_Type> get_file_extension(Template_String<Char_Type> file_path)
{
	for (int i = file_path.length - 1; i >=0; i--)
	{
		char c = file_path.data[i];

		if (c == '.')
		{
			return file_path.sliced(i + 1, file_path.length - i - 1);
		}
	}
	return file_path;
}

template <typename Char_Type>
Template_String<Char_Type> get_path_without_file_name(Template_String<Char_Type> file_path)
{
	for (int i = file_path.length - 1; i >=0; i--)
	{
		char c = file_path.data[i];

		if (is_path_separator(c))
		{
			return file_path.sliced(0, i);
		}
	}
	return Template_String<Char_Type>::empty;
}



inline void append_path_piece(String_Builder<char32_t>& builder, Unicode_String str)
{
	str.trim();

	if (str.length < 1) return;

	if (is_path_separator(str[0]))
		str.advance(1);

	if (is_path_separator(str[str.length - 1]))
		str.slice(0, str.length - 1);

	builder.append(str);
	builder.append(path_separator);
};

template <int N>
inline void append_path_piece(String_Builder<char32_t>& builder, const char32_t (&str_literal)[N])
{
	Unicode_String str = str_literal;

	str.trim();

	if (str.length < 1) return;

	if (is_path_separator(str[0]))
		str.advance(1);

	if (is_path_separator(str[str.length - 1]))
		str.slice(0, str.length - 1);

	builder.append(str);
	builder.append(path_separator);
};

inline void append_path_piece(String_Builder<char32_t>& builder)
{

}

template <typename ...Types>
inline void append_path_piece(String_Builder<char32_t>& builder, Unicode_String str, Types const... args)
{
	append_path_piece(builder, str);
	append_path_piece(builder, args...);
};

template <typename ...Types>
inline Unicode_String path_concat(Allocator allocator, Types const... args)
{
	String_Builder<char32_t> builder = build_string<char32_t>(allocator, 128);

	append_path_piece(builder, args...);

	Unicode_String result = builder.get_string();

	// This could been avoided if we knew the count of arguments, but we're in C++.
	//  We could do that, but i don't want to write 1000 more template functions
	if (is_path_separator(result[result.length - 1]))
	{
		assert(result.length > 1);
		result.length -= 1;
	}

	return result;
}



enum File_Open_Mode : u32
{
	FILE_READ = 1,
	FILE_WRITE = 1 << 1,
	FILE_CREATE_NEW = 1 << 3,

	FILE_APPEND = 1 << 4,

	FILE_EOF = 1 << 17, // :FileEofFlag
};

inline File_Open_Mode operator|(File_Open_Mode a, File_Open_Mode b)
{
	return (File_Open_Mode) ( ((u32) a) | ((u32) b) );
}




struct File
{
	Allocator allocator;

	u8* buffer;
	size_t buffer_size;
	size_t buffer_occupied;
	size_t buffer_read; // Used by read mode to determine how much of the buffer has been already read.

	Unicode_String file_path;


	File_Open_Mode open_mode; // :FileEofFlag: The eof flag is stored here just to save some space.

#if OS_WINDOWS
	HANDLE windows_handle;
#elif OS_LINUX
	int linux_handle;
#else
	static_assert(false);
#endif


	bool succeeded_to_open()
	{
	#if OS_WINDOWS
		return windows_handle != INVALID_HANDLE_VALUE;
	#elif OS_LINUX
		return dir != NULL;
	#else
		static_assert(false);
	#endif
	}

	inline bool eof()
	{
		return (open_mode & FILE_EOF) ? true : false;
	}


	inline void close()
	{
		assert(succeeded_to_open());

		if (open_mode & FILE_WRITE)
		{
			os_write_and_flush(buffer, buffer_occupied);
		}

		allocator.free(file_path.data, code_location());

	#if OS_WINDOWS
		CloseHandle(windows_handle);
		windows_handle = INVALID_HANDLE_VALUE;
	#elif OS_LINUX
		close(linux_handle);
		linux_handle = NULL;
	#else
		static_assert(false);
	#endif
	}

	inline void seek(s64 delta)
	{
		if (delta == 0) return;

		if (delta < 0)
		{
			if (buffer_read >= -delta)
			{
				buffer_read += delta;
				return;
			}
		}
		else if (delta <= buffer_occupied - buffer_read)
		{
			buffer_read += delta;
			return;
		}


		buffer_occupied = 0;

		#if OS_WINDOWS
		LARGE_INTEGER li;
		li.QuadPart = delta;
		bool success = SetFilePointerEx(windows_handle, li, NULL, FILE_CURRENT);
		if (!success)
		{
			abort_the_mission(U"Failed to SetFilePointerEx. GetLastError(): %", GetLastError());
		}
		#elif 
		static_assert(false);
		#endif
	}


	inline void os_write_and_flush(u8* write_buffer, size_t write_buffer_size)
	{
	#if OS_WINDOWS
		int write_result = WriteFile(windows_handle, write_buffer, write_buffer_size, NULL, NULL);
		//int flush_result = FlushFileBuffers(windows_handle);
		assert(write_result);// @TODO: do better check that works always
		//assert(flush_result);// @TODO: do better check that works always
	#elif
		ssize_t wrote_bytes = write(linux_handle, write_buffer, write_buffer_size);
		assert(wrote_bytes == write_buffer_size); // @TODO: do better check that works always
		int fsync_result = fsync(linux_handle);
		assert(fsync_result == 0);// @TODO: do better check that works always
	#else
		static_assert(false);
	#endif
	}

	inline void flush()
	{
		os_write_and_flush(buffer, buffer_occupied);
		buffer_occupied = 0;
	}

	inline void write(void* write_buffer, size_t length)
	{
		assert(open_mode & FILE_WRITE);

		if (buffer_size != 0)
		{
			while (length > 0)
			{
				size_t buffer_remaining = buffer_size - buffer_occupied;

				if (length > buffer_remaining)
				{
					memcpy(buffer + buffer_occupied, write_buffer, buffer_remaining);
					length       -= buffer_remaining;
					write_buffer  = add_bytes_to_pointer(write_buffer, buffer_remaining);

					buffer_occupied = buffer_size;
					flush();
					continue;
				}
				else
				{
					memcpy(buffer + buffer_occupied, write_buffer, length);
					buffer_occupied += length;
					return;
				}
			}
		}
		else
		{
			os_write_and_flush((u8*) write_buffer, length);
		}
	}

	inline void write(String str)
	{
		write((u8*) str.data, str.length);
	}


	inline void os_read(u8* read_buffer, size_t read_buffer_size, size_t* read_length)
	{
		assert(open_mode & FILE_READ);

	#if OS_WINDOWS
		// @TODO: check if succeeds
		DWORD windows_read_length;

		bool success = ReadFile(windows_handle, read_buffer, read_buffer_size, &windows_read_length, NULL);
		assert(success);

		*read_length = (size_t) windows_read_length;
	#elif OS_LINUX

		ssize_t read_size = read(linux_handle, read_buffer, read_buffer_size);
		assert(read_size != -1); // @TODO: do better check

		*read_length = (size_t) read_size;

	#endif
	}

	inline size_t read(u8* read_buffer, size_t read_buffer_size)
	{
		size_t read = 0;

		while (read_buffer_size > 0)
		{
			size_t buffer_remaining = buffer_occupied - buffer_read;

			if (buffer_remaining > read_buffer_size)
			{
				memcpy(read_buffer + read, buffer + buffer_read, read_buffer_size);
				buffer_read += read_buffer_size;
				read        += read_buffer_size;
				return read;
			}
			else
			{
				memcpy(read_buffer + read, buffer + buffer_read, buffer_remaining);
				read_buffer_size -= buffer_remaining;
				read += buffer_remaining;

				size_t os_read_length;
				os_read(buffer, buffer_size, &os_read_length);
				if (os_read_length == 0)
				{
					buffer_read = buffer_occupied;
					open_mode = (File_Open_Mode) (open_mode | FILE_EOF);
					return read;
				}

				buffer_occupied = os_read_length;
				buffer_read = 0;

				continue;
			}
		}

		return read;
	}

	inline bool read(u8* byte)
	{
		if (buffer_read < buffer_occupied)
		{
			*byte = buffer[buffer_read];
			buffer_read += 1;
			return true;
		}

		size_t os_read_length;
		os_read(buffer, buffer_size, &os_read_length);
		buffer_occupied = os_read_length;
		buffer_read = 0;
		if (os_read_length == 0)
		{
			open_mode = (File_Open_Mode) (open_mode | FILE_EOF);
			return false;
		}

		*byte = buffer[0];
		buffer_read += 1;
		return true;
	}



	inline u64 skip_until(u8 value)
	{
		u64 skipped_length = 0;

		u8 b;
		while (read(&b))
		{
			if (b == value) break;

			skipped_length += 1;
		}

		return skipped_length;
	}

	inline u8* read_until(u8 value, u64* out_read_length, Allocator allocator)
	{
		assert(open_mode & FILE_READ);

		u64 read_length = 0;
		u64 buffer_size = 128;

		u8* buffer = (u8*) allocator.alloc(buffer_size, code_location());

		u8 b;
		while (read(&b))
		{
			if (read_length == buffer_size)
			{
				buffer_size *= 2;
				buffer = (u8*) allocator.realloc(buffer, buffer_size / 2, buffer_size, code_location());
			}

			buffer[read_length] = b;

			read_length += 1;

			if (b == value) break;
		}

		return buffer;
	}

	inline u8* read_until(Dynamic_Array<char> chars, u64* out_read_length, Allocator allocator)
	{
		assert(open_mode & FILE_READ);

		u64 read_length = 0;
		u64 buffer_size = 128;

		u8* buffer = (u8*) allocator.alloc(buffer_size, code_location());

		u8 b;
		while (read(&b))
		{
			if (read_length == buffer_size)
			{
				buffer_size *= 2;
				buffer = (u8*) allocator.realloc(buffer, buffer_size / 2, buffer_size, code_location());
			}

			buffer[read_length] = b;

			read_length += 1;

			if (chars.contains(b)) break;
		}

		return buffer;
	}



	inline String read_line(Allocator allocator)
	{
		u64 read_length;
		u8* line = read_until('\n', &read_length, allocator);

		return String((char*) line, read_length);
	}

	inline u64 size()
	{
		assert(succeeded_to_open());

	#if OS_WINDOWS

		DWORD high_bits;
		DWORD low_bits;

		low_bits = GetFileSize(windows_handle, &high_bits);

		return (u64(low_bits) | (u64(high_bits) << 32));

	#elif OS_LINUX

		stat st;

		fstat(linux_handle, &st);
		return st.size;

	#else
		static_assert(false)
		return 0;
	#endif
	}
};

// If you do not specify either to read or write a file, buffer will not allocated.
inline File open_file(Allocator allocator, Unicode_String path, File_Open_Mode open_mode, size_t buffer_size = 4 * 1024)
{
	File file;

	file.allocator = allocator;
	file.open_mode = open_mode;
	file.buffer = NULL;


	// Can't have the file to read and written at the same time
	if ((open_mode & FILE_READ) && (open_mode & FILE_WRITE))
	{
		return file;
	}


#if OS_WINDOWS

	file.windows_handle = INVALID_HANDLE_VALUE;

	int wide_name_length;
	char16_t* wide_name = path.to_utf16(allocator, &wide_name_length);
	defer { allocator.free(wide_name, code_location()); };

	int windows_open_flags;
	if (open_mode & FILE_READ)
		windows_open_flags = GENERIC_READ;
	else if (open_mode & FILE_WRITE)
		windows_open_flags = GENERIC_WRITE;
	else
		windows_open_flags = 0;

#if 0
	int share_mode = 0;
	if (!(open_mode & FILE_WRITE))
	{
		share_mode = FILE_SHARE_READ;
	}
#else
	int share_mode = FILE_SHARE_READ;
#endif

	int creation_disposition;

	if (open_mode & FILE_CREATE_NEW)
		creation_disposition = CREATE_ALWAYS;
	else
		creation_disposition = OPEN_EXISTING;


	file.windows_handle = CreateFileW((wchar_t*) wide_name, windows_open_flags, share_mode, NULL, creation_disposition, FILE_ATTRIBUTE_NORMAL, NULL);

#if DEBUG
	if (file.windows_handle == INVALID_HANDLE_VALUE)
	{
		log(ctx.logger, U"Failed to open file: %. GetLastError() = %", path, GetLastError());
	}
#endif

	if (file.succeeded_to_open() && open_mode & (FILE_WRITE | FILE_APPEND))
	{
		auto set_file_pointer_result = SetFilePointer(file.windows_handle, 0, NULL, FILE_END);
		assert(set_file_pointer_result != INVALID_SET_FILE_POINTER);
	}

#elif OS_LINUX

	file.linux_handle = -1;

	int utf8_name_length;
	char* utf8_name = path.to_utf8(allocator, &utf8_name_length);
	defer { allocator.free(utf8_name, code_location()); };


	int oflag = 0;

	if (open_mode & FILE_READ)
		o_flag = O_RDONLY;
	else if (open_mode & FILE_WRITE)
		o_flag = O_WRONLY;

	if (open_mode & FILE_CREATE_NEW)
		o_flag |= O_TRUNC;

	if (o_flag == 0)
	{
		// Caller wants to check if file exists
		o_flag = O_WRONLY | O_CREAT | O_EXCL;
	}

	file.linux_handle = open(utf8_name, o_flag);

	if (file.succeded_to_open() && open_mode & (FILE_WRITE | FILE_APPEND))
	{
		lseek(file.linux_handle, 0, SEEK_END);
	}


#else
	static_assert(false);
#endif

	if (file.succeeded_to_open() && (open_mode | (FILE_READ | FILE_WRITE)))
	{
		file.buffer_occupied = 0;
		file.buffer_read = 0;
		file.buffer_size = buffer_size;
		file.buffer = (u8*) allocator.alloc(buffer_size, code_location());

		file.file_path = path.copy_with(allocator);
	}

	return file;
}

inline bool does_file_exist(Allocator allocator, Unicode_String path)
{
	File file = open_file(allocator, path, (File_Open_Mode) 0);
	defer { file.close(); };

	return file.succeeded_to_open();
}


inline bool read_entire_file_to_buffer(Allocator allocator, Unicode_String file_path, Buffer* out_result)
{
	File file = open_file(allocator, file_path, FILE_READ);

	if (!file.succeeded_to_open())
	{
		return false;
	}

	u64 file_size = file.size();

	Buffer read_buffer = create_buffer(file_size, allocator);

	u64 read = file.read((u8*) read_buffer.data, file_size);
	assert(read == file_size);

	read_buffer.occupied = file_size;

	file.close();

	*out_result = read_buffer;
	return true;
}



inline bool create_directory_recursively(Unicode_String path)
{
	assert(false);
}