#pragma once
#include "Basic.h"
#include "Allocator.h"
#include "Log.h"


#if OS_WINDOWS
#include <intrin.h>
#include <windows.h>

typedef int Thread_Id;
#endif

typedef uint (_stdcall Thread_Proc)(void* ptr);

struct Threading
{
	Thread_Id main_thread_id = current_thread_id();

	inline void sleep(u32 ms)
	{
	#if OS_WINDOWS
		Sleep(ms);
	#endif
	}

	inline void yield()
	{
		sleep(0);
	}

	inline Thread_Id current_thread_id()
	{
	#if OS_WINDOWS
		return (Thread_Id) GetCurrentThreadId();
	#endif
	}

	inline bool is_main_thread()
	{
		return current_thread_id() == main_thread_id;
	}
};

inline Threading threading;

struct Thread
{
	void* data_pointer;

#if OS_WINDOWS
	HANDLE windows_handle;
#endif

	Thread() {};


	inline bool is_finished()
	{
		DWORD code;
		if (GetExitCodeThread(windows_handle, &code))
		{
			return code != STILL_ACTIVE;
		}
		assert(false);
		return false;
	}

	inline bool wait_for_finish()
	{
		auto result = WaitForSingleObject(windows_handle, INFINITE);
		if (result == WAIT_FAILED)
		{
			assert(false);
		}
		return true;
	}
};


typedef void (THREAD_PROC)(void*);

// Pass the thread-safe allocator !!
inline Thread create_thread(Allocator allocator, void (thread_proc_arg)(void*), void* data_pointer)
{
	if (!(allocator.flags & ALLOCATOR_THREAD_SAFE))
	{
		abort_the_mission(U"create_thread requires thread-safe allocator");
		return Thread();
	}	


	struct Win_Thread_Data
	{
		decltype(thread_proc_arg) thread_proc;
		void* data_ptr;
		Allocator allocator;
	};

	Win_Thread_Data* win_thread_data = (Win_Thread_Data*) allocator.alloc(sizeof(Win_Thread_Data), code_location());

	win_thread_data->thread_proc = thread_proc_arg;
	win_thread_data->data_ptr    = data_pointer;
	win_thread_data->allocator   = allocator;

	Thread_Proc* win_thread_proc = [](void* data_ptr) -> u32
	{
		Win_Thread_Data* win_thread_data = (Win_Thread_Data*) data_ptr;

		win_thread_data->thread_proc(win_thread_data->data_ptr);

		win_thread_data->allocator.free(data_ptr, code_location());

		return 0;
	};

	Thread thread;

	thread.data_pointer = (void*) win_thread_data;
	DWORD thread_id;
	thread.windows_handle = CreateThread(0, 0, (LPTHREAD_START_ROUTINE) win_thread_proc, thread.data_pointer, 0, (LPDWORD) &thread_id);

	if (!thread.windows_handle)
	{
		abort_the_mission(U"CreateThread failed");
		return Thread();
	}

	return thread;
}



struct Mutex
{
#if OS_WINDOWS
	CRITICAL_SECTION critical_section = {  };
#else
	static_assert(false);
#endif

	inline void lock()
	{
		EnterCriticalSection(&critical_section);
	}

	inline void unlock()
	{
		LeaveCriticalSection(&critical_section);
	}

	inline bool try_lock()
	{
		return TryEnterCriticalSection(&critical_section);
	}

	inline void free()
	{
		DeleteCriticalSection(&critical_section);
	}
};


inline Mutex create_mutex()
{
	Mutex mutex;

	InitializeCriticalSection(&mutex.critical_section);

	return mutex;
}

struct Scoped_Lock
{
	Mutex& _mutex;

	Scoped_Lock(Mutex& mutex): _mutex(mutex)
	{
		_mutex.lock();
	}

	~Scoped_Lock()
	{
		_mutex.unlock();
	}
};


struct Semaphore
{
	HANDLE windows_handle;

	inline void wait_and_decrement()
	{
		u32 result = WaitForSingleObject(windows_handle, INFINITE);
		assert(result == WAIT_OBJECT_0);
	}

	inline u32 increment(s32 value = 1)
	{
		long previous_count;
		bool result = ReleaseSemaphore(windows_handle, value, &previous_count);
		assert(result);

		return previous_count;
	}

	inline void reset()
	{
		// @Cleanup
		//  WTFF. Probably just remove this crap from here. Not sure how
		//    this would work on other OSes than Windows.
		//    And i'm also not sure if it makes sense to use semaphore like this
		//    probably you should've switched to conditional variable or other sync. primitive.
		while (true)
		{
			WaitForSingleObject(windows_handle, 0);
			long semaphore_count;
			ReleaseSemaphore(windows_handle, 0, &semaphore_count);
			
			if (semaphore_count == 0) return;
		}
	}

	inline void free()
	{
		CloseHandle(windows_handle);
	}
};

inline Semaphore create_semaphore(s32 initial_value = 0)
{
	Semaphore semaphore;

	semaphore.windows_handle = CreateSemaphoreA(NULL, initial_value, LONG_MAX, NULL);

	return semaphore;
}

	
	




template <typename T>
inline T atomic_set(volatile T* target, T value)
{
	return _InterlockedExchange(target, value);
}

template <typename T>
inline T atomic_add(volatile T* target, T to_add)
{
	return InterlockedExchangeAdd(target, to_add);
}

template <typename T>
inline T atomic_increment(volatile T* target)
{
	return _InterlockedIncrement(target);
}

template <typename T>
inline T atomic_decrement(volatile T* target)
{
	return _InterlockedDecrement(target);
}

