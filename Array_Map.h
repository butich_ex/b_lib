#pragma once

#include "Basic.h"
#include "Allocator.h"
#include "Dynamic_Array.h"

template <typename K, typename T>
struct Array_Map
{
    Dynamic_Array<K> keys;
    Dynamic_Array<T> values;
    
    Allocator allocator;
    
    Array_Map(int capacity, Allocator allocator)
    {
        this->allocator = allocator;
        
        keys   = Dynamic_Array<K>(capacity, allocator);
        values = Dynamic_Array<T>(capacity, allocator);
    }

	Array_Map() {}

    // Pointer will get invalid on the event of resize.
    T* put(const K key)
    {
        keys.add(key);
        return values.add();
    }
    
    // Pointer will get invalid on the event of resize.
    T* put(const K key, const T item)
    {
        auto ptr = put(key);
        *ptr = item;
        return ptr;
    }
    
    T* get(const K key)
    {
        auto index_of = keys.index_of(key);
		if (index_of == -1) return nullptr;
        return values[index_of];
    }
    
    bool remove(const K key)
    {
        auto index_of = keys.index_of(key);
        if (index_of == -1)
        {
            return false;
        }
        else
        {
            keys  .remove_at_index(index_of);
            values.remove_at_index(index_of);
            return true;
        }
    }

	void free()
	{
		keys.free();
		values.free();
	}
};
