#pragma once

#include "String_Builder.h"

template <typename Char_Type>
struct Text_Editor
{
	String_Builder<Char_Type>* builder;
	int cursor;
	int selection_length;

	inline void append_at_cursor(Char_Type c)
	{
		maybe_delete_selection();
		
		builder->append(cursor, c);
		cursor += 1;
	}

	inline bool maybe_delete_selection()
	{
		if (selection_length == 0) return false;

		int left_selection_position = min(cursor, cursor + selection_length);

		builder->remove(left_selection_position, abs(selection_length));

		cursor = left_selection_position;
		selection_length = 0;

		return true;
	}

	inline bool delete_before_cursor()
	{
		if (maybe_delete_selection()) return false;

		if (cursor <= 0) return false;
		
		cursor -= 1;
		builder->remove(cursor, 1);

		return true;
	}

	inline bool delete_after_cursor()
	{
		if (maybe_delete_selection()) return false;

		if (cursor >= builder->length) return false;

		builder->remove(cursor, 1);

		return true;
	}

	inline void move_cursor(int delta)
	{
		if (selection_length)
		{
			int delta_sign = sign(delta);

			if (delta_sign == 0) return;


			int selection_side = cursor + selection_length;
			
			if (delta > 0)
				cursor = max(selection_side, cursor);  // Cursor is just another selection side
			else
				cursor = min(selection_side, cursor); 

			selection_length = 0;

			// Move the remaining delta. delta_sign is -1 or 1 always.
			move_cursor(delta - delta_sign);
		}
		else
		{
			cursor = clamp(0, builder->length, cursor + delta);
		}
	}

	inline void next_word(int direction, bool keep_selecting)
	{
		if (direction >= 0)
		{
			if (!keep_selecting)
			{
				cursor = min(cursor + selection_length, cursor);
				selection_length = 0;
			}


			int old_cursor = cursor;

			while (true)
			{
				if (cursor >= builder->length) break;

				Char_Type c = builder->buffer[cursor];
				if (!is_whitespace(c))
				{
					break;
				}
				cursor += 1;
			}

			while (true)
			{
				if (cursor >= builder->length) break;

				Char_Type c = builder->buffer[cursor + 1];

				cursor += 1;

				if ((is_ascii(c) && ispunct(c)) || is_whitespace(c) || c == '\n')
				{
					break;
				}
			}

			if (keep_selecting)
			{
				selection_length -= (cursor - old_cursor);
			}
		}
		else
		{
			if (!keep_selecting)
			{
				cursor = max(cursor + selection_length, cursor);
				selection_length = 0;
			}


			int old_cursor = cursor;

			while (true)
			{
				if (cursor <= 0) break;

				Char_Type c = builder->buffer[cursor - 1];				
				if (!is_whitespace(c))
				{
					break;
				}
				cursor -= 1;
			}

			while (true)
			{
				if (cursor <= 0) break;

				Char_Type c = builder->buffer[cursor - 1];

				cursor -= 1;

				if ((is_ascii(c) && ispunct(c)) || is_whitespace(c) || c == '\n')
				{
					break;
				}
			}

			if (keep_selecting)
			{
				selection_length -= (cursor - old_cursor);
			}
		}
	}



	inline void advance_selection(int delta)
	{
		int old_cursor = cursor;
		cursor = clamp(0, builder->length, cursor + delta);
		selection_length -= cursor - old_cursor;
	}

	inline void clear()
	{
		builder->clear();

		cursor = 0;
		selection_length = 0;
	}

	inline void copy_from(Template_String<Char_Type> str)
	{
		clear();
		
		builder->append(str);
	}



	inline Template_String<Char_Type> get_string()
	{
		return builder->get_string();
	}

	inline Template_String<Char_Type> get_selected_string()
	{
		if (selection_length == 0) return Template_String<Char_Type>::empty;

		Template_String<Char_Type> str = get_string();

		if (selection_length < 0)
			str.slice(cursor + selection_length, -selection_length);
		else
			str.slice(cursor, selection_length);

		return str;
	}
};


template <typename Char_Type>
inline Text_Editor<Char_Type> create_text_editor(String_Builder<Char_Type>* builder)
{
	Text_Editor<Char_Type> editor;
	
	editor.builder = builder;
	editor.cursor = 0;
	editor.selection_length = 0;

	return editor;
}
