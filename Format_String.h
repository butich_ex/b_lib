#pragma once

#include "String.h"
#include "String_Builder.h"
#include "String_Conversion.h"
#include "String_Builder.h"


template <typename Char_Type>
inline void format_string_internal(Allocator allocator, String_Builder<Char_Type>* builder, Template_String<Char_Type> format)
{
	builder->append(format);
}

template <typename Char_Type, typename T, typename... Types>
inline void format_string_internal(Allocator allocator, String_Builder<Char_Type>* builder, Template_String<Char_Type> format, T first_value, Types const... args)
{
	for (int i = 0; i < format.length; i++)
	{
		const auto c = format[i];
		if (c == '%')
		{
			builder->append(format.sliced(0, i));
			
			if constexpr (std::is_same_v<Char_Type, char>)
			{
				static_assert(!std::is_same_v<T, Unicode_String>);
			}

			if constexpr (std::is_same_v<Char_Type, char32_t> && std::is_same_v<T, String>)
			{
				Unicode_String unicode_str = Unicode_String::from_ascii(first_value, allocator);
				builder->append(unicode_str);
				allocator.free(unicode_str.data, code_location());
			}
			else if constexpr (std::is_same_v<T, Template_String<Char_Type>>)
			{
				builder->append(first_value);
			}
			else if constexpr (std::is_same_v<T, const char*> || std::is_same_v<T, char*>)
			{
				const char* ptr = first_value;
				while (*ptr != '\0')
				{
					builder->append((Char_Type) *ptr);
					ptr += 1;
				}
			}
			else if constexpr (std::is_same_v<T, char> || std::is_same_v<T, char32_t>)
			{
				builder->append((Char_Type) first_value);
			}
			else
			{
				String str = to_string(first_value, allocator);
				if constexpr (std::is_same_v<Char_Type, char32_t>)
				{
					Unicode_String unicode_str = Unicode_String::from_ascii(str, allocator);
					builder->append(unicode_str);
					allocator.free(unicode_str.data, code_location());
				}
				else
				{
					builder->append(str);
				}
			}
			format_string_internal(allocator, builder, format.advanced(i + 1), args...);
			return;
		}
	}

	builder->append(format);
}

template <typename Char_Type, typename... Types>
inline Template_String<Char_Type> format_string_impl(Allocator allocator, Template_String<Char_Type> format, Types const... args)
{
	String_Builder<Char_Type> builder = build_string<Char_Type>(allocator, 64);

	format_string_internal(allocator, &builder, format, args...);

	return builder.get_string();
}


template <typename... Types>
inline String format_string(Allocator allocator, String format, Types const... args)
{
	return format_string_impl(allocator, format, args...);
}

template <typename... Types>
inline Unicode_String format_unicode_string(Allocator allocator, Unicode_String format, Types const... args)
{
	return format_string_impl(allocator, format, args...);
}






inline String size_to_string(u64 size, Allocator allocator)
{
	if (size < 1024)
	{
		return format_string(allocator, "% bytes", size);
	}
	else if (size < 1024 * 1024)
	{
		return format_string(allocator, "% kilobytes", size / 1024);
	}
	else
	{
		return format_string(allocator, "% megabytes", size / 1024 / 1024);
	}
}