#pragma once
#include "Basic.h"
#include <cstdlib>



enum class Allocator_Proc_Mode
{
	Allocate = 0,
	Realloc = 1,
	Free = 2,
};


typedef void* ALLOCATOR_PROC(Allocator_Proc_Mode mode, void* old_data, size_t old_size, size_t size, void* allocator_data, Code_Location code_location);


// You should pass allocator by value.


// I'm not sure about passing Code_Location every time.
// It might hurt performance. I maybe should've made a macro
// with condition, so i can enable passing it only when i really need.

const u32 ALLOCATOR_HAS_NO_FREE_AND_REALLOC = 0;
const u32 ALLOCATOR_HAS_REALLOC = 1;
const u32 ALLOCATOR_HAS_FREE    = 1 << 1;
const u32 ALLOCATOR_THREAD_SAFE = 1 << 2;

struct Allocator
{
	ALLOCATOR_PROC* proc;
	void* allocator_data;
	u32 flags;

#ifdef ALLOCATOR_NAMES
	const char* name;
#endif 




	inline void* alloc(size_t size, Code_Location code_location) const
	{
#ifdef TRACY_ENABLE
		ZoneScopedC(0xff8000);
		
		char allocation_name[64];
		ZoneName(allocation_name, sprintf(allocation_name, "Alloc: %s %llu", name, size));

		TracyPlot("Allocation size", size);
		TracyPlotConfig("Allocation size", tracy::PlotFormatType::Memory);
#endif
		return proc(Allocator_Proc_Mode::Allocate, NULL, 0, size, allocator_data, code_location);
	}

	inline void free(void* data, Code_Location code_location) const
	{
		// Just ignore this. Nothing terrible is going to happen.
		// assert(flags & ALLOCATOR_HAS_FREE);
#ifdef TRACY_ENABLE
		ZoneScopedC(0xff8000);

		char allocation_name[64];
		ZoneName(allocation_name, sprintf(allocation_name, "Free: %s", name));
#endif

		proc(Allocator_Proc_Mode::Free, data, 0, 0, allocator_data, code_location);
	}

	inline void* realloc(void* data, size_t old_size, size_t new_size, Code_Location code_location) const
	{
#ifdef TRACY_ENABLE
		ZoneScopedC(0xff8000);

		char allocation_name[128];
		ZoneName(allocation_name, sprintf(allocation_name, "Realloc: %s  %llu -> %llu", name, old_size, new_size));


		TracyPlot("Old size", old_size);
		TracyPlotConfig("Old size", tracy::PlotFormatType::Memory);

		TracyPlot("New size", new_size);
		TracyPlotConfig("New size", tracy::PlotFormatType::Memory);
#endif

		void* result;
		if (flags & ALLOCATOR_HAS_REALLOC)
		{
			result = proc(Allocator_Proc_Mode::Realloc, data, old_size, new_size, allocator_data, code_location);
		}
		else
		{
			void* new_data = alloc(new_size, code_location);
			memcpy(new_data, data, old_size);
			free(data, code_location);
			result = new_data;
		}

		return result;
	}



	inline bool operator==(Allocator other) const
	{
		return proc == other.proc && allocator_data == other.allocator_data;
	}

	inline bool operator!=(Allocator other) const
	{
		return !this->operator==(other);
	}
};



inline void* malloc_crash_on_failure(size_t size)
{
	void* result = malloc(size);
	if (!result)
	{
		assert_msg(false, "Failed to malloc()");
		exit(-1);
	}
	return result;
}

inline void* realloc_crash_on_failure(void* data, size_t size)
{
	void* result = realloc(data, size);
	if (!result)
	{
		assert_msg(false, "Failed to realloc()");
		exit(-1);
	}
	return result;
}

inline void* c_allocator_proc(Allocator_Proc_Mode mode, void* old_data, size_t old_size, size_t size, void* allocator_data, Code_Location code_location)
{
	switch (mode)
	{
		case Allocator_Proc_Mode::Allocate:
		{
			return malloc_crash_on_failure(size);
		}
		break;
		case Allocator_Proc_Mode::Realloc:
		{
			return realloc_crash_on_failure(old_data, size);
		}
		break;
		case Allocator_Proc_Mode::Free:
		{
			free(old_data);
		}
		break;


		default: assert(false);
	}

	return NULL;
}


const Allocator c_allocator = {
	.proc = &c_allocator_proc,
	.allocator_data = NULL,
	.flags = ALLOCATOR_HAS_FREE | ALLOCATOR_HAS_REALLOC | ALLOCATOR_THREAD_SAFE,

#ifdef ALLOCATOR_NAMES
	.name = "c_allocator",
#endif
};

const Allocator null_allocator = { 
	.proc = NULL,
	.allocator_data = NULL,
	.flags = 0,

#ifdef ALLOCATOR_NAMES
	.name = "null_allocator",
#endif
};



template <typename T>
T* allocate(int count, Allocator allocator)
{
	return reinterpret_cast<T*>(allocator.alloc(count * sizeof(T), code_location()));
}

template <typename T>
inline T* allocate_and_copy(T* thing, Allocator allocator)
{
	void* mem = allocator.alloc(sizeof(T), code_location());

	memcpy(mem, thing, sizeof(T));

	return (T*) mem;
}








struct Buffer
{
	void* data;

	u32 capacity;
	u32 occupied;

	Allocator allocator;


	inline void ensure_capacity(u32 target_capacity)
	{
		if (target_capacity > capacity)
		{
			u32 old_capacity = capacity;

			capacity *= 2;
			if (capacity < target_capacity)
			{
				capacity = target_capacity;
			}

			data = allocator.realloc(data, old_capacity, capacity, code_location());
		}

	}


	inline void append(void* append_data, u32 size)
	{
		ensure_capacity(occupied + size);

		memcpy(add_bytes_to_pointer(data, occupied), append_data, size);
		occupied += size;
	}

	template <typename T>
	inline void append(T item)
	{
		append(&item, sizeof(T));
	}
};

inline Buffer create_buffer(u32 initial_capacity, Allocator allocator)
{
	Buffer buffer;

	buffer.data = allocator.alloc(initial_capacity, code_location());
	buffer.capacity = initial_capacity;
	buffer.occupied = 0;
	buffer.allocator = allocator;

	return buffer;
}


