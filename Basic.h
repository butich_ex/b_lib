#pragma once

// Windows 10
#define WINVER 0x0A00
#define _WIN32_WINNT 0x0A00
#define NTDDI_VERSION 0x0A000000

#include <WinSDKVer.h>

#include <cstdio>
#include <cstdint>
#include <assert.h>
#include <cstring>
#include <type_traits>

template <class, template <class> class>
struct is_template_instance : public std::false_type {};

template <class T, template <class> class U>
struct is_template_instance<U<T>, U> : public std::true_type {};

template<typename T>
struct extract_value_type
{
    typedef T value_type;
};

template<template<typename> class X, typename T>
struct extract_value_type<X<T>>
{
    typedef T value_type;
};



#ifdef _WIN32
#define OS_WINDOWS 1
#else
#define OS_WINDOWS 0
#endif

#ifdef __linux__
#define OS_LINUX 1
#else
#define OS_LINUX 0
#endif


#if _DEBUG
#define DEBUG 1
#else
#define DEBUG 0
#endif


#define assert_msg(expr, msg) assert((expr) && (msg))


#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)

typedef unsigned int    uint;
typedef unsigned short  ushort;
typedef unsigned char   uchar;
typedef unsigned long   ulong;


typedef char s8;
typedef unsigned char u8;

typedef short s16;
typedef unsigned short u16;

typedef int s32;
typedef unsigned int u32;

typedef long long s64;
typedef unsigned long long u64;

typedef float f32;
typedef double f64;

typedef u32 b32;


constexpr s8 s8_max  = 0x7f;
constexpr s8 s8_min  = -s8_max - 1;

constexpr u8 u8_max  = 0xff;
constexpr u8 u8_min  = 0;

constexpr s16 s16_max = 0x7fff;
constexpr s16 s16_min = -s16_max - 1;

constexpr u16 u16_max = 0xffff;
constexpr u16 u16_min = 0;


constexpr s32 s32_max = 0x7fff'ffff;
constexpr s32 s32_min = -s32_max - 1;

constexpr u32 u32_max = 0xffff'ffff;
constexpr u32 u32_min = 0;


constexpr s64 s64_max = 0x7fff'ffff'ffff'ffff;
constexpr s64 s64_min = -s64_max - 1;

constexpr u64 u64_max = 0xffff'ffff'ffff'ffff;
constexpr u64 u64_min = 0;


constexpr s8  s8_sign_mask  = 1 << 7;
constexpr s16 s16_sign_mask = 1 << 15;
constexpr s32 s32_sign_mask = 1 << 31;
constexpr s64 s64_sign_mask = 1LL << 63;


constexpr inline size_t megabytes(size_t mb)
{
	return mb * 1024 * 1024;
}


template <typename T>
constexpr inline T percent(T value)
{
	return value / T(100);
}

#define CONCAT_INTERNAL(x,y) x##y
#define CONCAT(x,y) CONCAT_INTERNAL(x,y)

template<typename T>
struct ExitScope {
	T lambda;
	ExitScope(T lambda) :lambda(lambda) {}
	~ExitScope() { lambda(); }
	ExitScope(const ExitScope&);
private:
	ExitScope& operator =(const ExitScope&);
};

class ExitScopeHelp {
public:
	template<typename T>
	ExitScope<T> operator+(T t) { return t; }
};

#define defer const auto& CONCAT(defer__, __LINE__) = ExitScopeHelp() + [&]()

inline void* get_pointer_with_size_and_index(void* base_ptr, size_t item_size, int index)
{
	return ((char*)base_ptr) + (item_size * index);
}

template <typename T>
inline T* add_bytes_to_pointer(T* pointer, u64 memory_offset)
{
	return (T*) ((u64) pointer + memory_offset);
}


template <typename T>
inline bool is_power_of_two(T number)
{
	return (number & (number - 1)) == 0;
}

template <typename T>
inline bool is_aligned(T number, int alignment)
{
	return (number % alignment) == 0;
}

template <typename T>
inline T align(T number, int alignment)
{
	assert(alignment > 0 && is_power_of_two(alignment));

	if (is_aligned(number, alignment)) return number;

	T result = number + (alignment - (number % alignment));

	return result;
}

#define get_header_size( struct_addr, location_addr ) ((int) ((size_t) location_addr - (size_t) struct_addr ))

struct Code_Location
{
	int line;
	const char* file_name;

	Code_Location() {};

	Code_Location(int line, const char* file_name)
	{
		this->line = line;
		this->file_name = file_name;
	}
};

#define code_location() Code_Location( __LINE__, __FILE__ )

#define megabytes(mb) (mb * 1024 * 1024)

template <typename T>
struct Scoped_Thing
{
	T  previous;
	T& to_change;

	Scoped_Thing(T& to_change, T color): to_change(to_change)
	{
		previous = to_change;
		to_change = color;
	}

	~Scoped_Thing()
	{
		to_change = previous;
	}
};

#define scoped_set_and_revert( _var, value ) Scoped_Thing CONCAT(__scoped, __LINE__) ( _var, value );\



template <typename T>
inline void reverse(T* data, int length)
{
	for (int i = 0; i < (length / 2); i++)
	{
		T temp = data[i];
		data[i] = data[length - i - 1];
		data[length - i - 1] = temp;
	}
}


template <typename T>
inline T sign(T a)
{
	if (a < 0)
	{
		return -1;
	}
	else
	{
		return 1;
	}
}

#undef max
#undef min

template <typename T>
constexpr inline T max(T a, T b)
{
	if (a > b)
	{
		return a;
	}
	return b;
}

template <typename T>
constexpr inline T min(T a, T b)
{
	if (a < b)
	{
		return a;
	}
	return b;
}

template <typename T>
constexpr inline T clamp(T min, T max, T value)
{
	if (value < min) return min;
	if (value > max) return max;

	return value;
}


struct Registrar
{
	Registrar(void (register_function)())
	{
		register_function();
	}
};


template <typename T, size_t n>
constexpr size_t array_count(T(&)[n])
{
	return n;
}



template <typename T>
inline void swap(T* a, T* b)
{
	T temp = *a;
	*a = *b;
	*b = temp;
}