#pragma once

#include "Log.h"

struct Context
{
	Logger logger;

	Context() {};
	Context(Logger logger)
	{
		this->logger = logger;
	}
};

inline thread_local Context ctx = Context(os_logger);



// @TODO: @TODO: REMOVE ME!!! This is here because of header file conflicts.
//   Actually in here we rely on type table, but it seems to be broken if we call this, does this make sense???
inline void print_types_that_referenced_this_type(Reflection::Type* referenced_type)
{
	using namespace Reflection;

	auto get_sane_string = [](String str) -> String
	{
		if (str.length < 0 || str.length > 1000)
			return String::empty;

		return str;
	};
	
	log(ctx.logger, U"Types that reference type: %, id: %", get_sane_string(referenced_type->name), referenced_type->id);

	for (auto iter = iterate_types(); Type* type = iter.next();)
	{
		switch (type->kind)
		{
			case Type_Kind::Enum:
			{
				Enum_Type* enum_type = (Enum_Type*) type;
				if (enum_type->base_type == referenced_type)
				{
					log(ctx.logger, U"--- Enum: %", enum_type->name);
				}
			}
			break;

			case Type_Kind::Struct:
			{
				Struct_Type* struct_type = (Struct_Type*) type;

				for (auto members = struct_type->iterate_members(R_allocator); Struct_Member* member = members.next();)
				{
					//log(ctx.logger, U"%.%", struct_type->name, member->name);

					if (member->type == referenced_type)
					{
						log(ctx.logger, U"--- Member of struct '%' named: %", struct_type->name, member->name);
					}
				}
			}
			break;

			case Type_Kind::Pointer:
			{
				Pointer_Type* pointer_type = (Pointer_Type*) type;
				if (pointer_type->inner_type == referenced_type)
				{
					log(ctx.logger, U"--- Pointer type with id: %", type->id);
				}
			}
			break;

			case Type_Kind::Array:
			{
				Array_Type* array_type = (Array_Type*) type;
				if (array_type->inner_type == referenced_type)
				{
					log(ctx.logger, U"--- Array type with id: %", type->id);
				}
			}
		}
	}
};
