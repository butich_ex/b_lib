#pragma once

#include "String.h"
#include <Windows.h>


template <typename Char_Type>
inline constexpr UINT window_clipboard_format_for_string_type()
{
	if constexpr (std::is_same_v<Char_Type, char>)
	{
		return CF_TEXT;
	}
	else
	{
		return CF_UNICODETEXT;
	}
}

template <typename Char_Type>
inline Template_String<Char_Type> get_os_clipboard(Allocator allocator)
{
	constexpr UINT windows_clipboard_format = window_clipboard_format_for_string_type<Char_Type>();

	if (!IsClipboardFormatAvailable(windows_clipboard_format) || !OpenClipboard(NULL))
	{
		return Template_String<Char_Type>::empty;
	}

	defer { CloseClipboard(); };

	auto hglb = GetClipboardData(windows_clipboard_format);
	if (hglb)
	{
		void* zero_terminated_str = GlobalLock(hglb);
		if (zero_terminated_str)
		{
			if constexpr (std::is_same_v<Char_Type, char>)
			{
				return String::from_c_string((char*) zero_terminated_str, allocator);
			}
			else
			{
				return Unicode_String::from_utf16((char16_t*) zero_terminated_str, allocator);
			}

			GlobalUnlock(hglb);
		}
	}

	return Template_String<Char_Type>::empty;
}


template <typename Char_Type>
void copy_to_os_clipboard(Template_String<Char_Type> str, Allocator allocator)
{
	constexpr UINT windows_clipboard_format = window_clipboard_format_for_string_type<Char_Type>();

	HGLOBAL global_mem;

	if constexpr (std::is_same_v<Char_Type, char32_t>)
	{
		int utf16_length;
		char16_t* utf16_str = str.to_utf16(allocator, &utf16_length);

		global_mem = GlobalAlloc(GMEM_MOVEABLE, sizeof(char16_t) * utf16_length);
		char16_t* buffer = (char16_t*) GlobalLock(global_mem);
		memcpy(buffer, utf16_str,  sizeof(char16_t) * utf16_length);
		// utf16_str is already zero terminated

		allocator.free(utf16_str, code_location());
	}
	else
	{
		HGLOBAL global_mem =  GlobalAlloc(GMEM_MOVEABLE, str.length + 1);
		char* buffer = (char*) GlobalLock(global_mem);
		memcpy(buffer, str.data, str.length);
		buffer[str.length] = '\0';
	}


	GlobalUnlock(global_mem);
	OpenClipboard(0);
	EmptyClipboard();
	SetClipboardData(windows_clipboard_format, global_mem);
	CloseClipboard();
}