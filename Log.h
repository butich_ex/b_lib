#pragma once

#include "Basic.h"
#include "Allocator.h"
#include "String.h"
#include "Format_String.h"

#if OS_WINDOWS
#include <Windows.h>
#endif


typedef void Log_Proc(Unicode_String str);

struct Logger
{
	Allocator concat_allocator;
	Log_Proc* log_proc;

	Logger() {};
};

inline Logger create_logger(Allocator concat_allocator, Log_Proc* log_proc)
{
	Logger logger;

	logger.concat_allocator = concat_allocator;
	logger.log_proc = log_proc;

	return logger;
}


inline void os_logger_proc(Unicode_String str)
{
	int utf8_length;

	char* utf8 = str.to_utf8(c_allocator, &utf8_length);

	fwrite(utf8, utf8_length, 1, stdout);
	fwrite("\n", 1, 1, stdout);

	c_allocator.free(utf8, code_location());
}


inline Logger os_logger = create_logger(c_allocator, &os_logger_proc);



template <typename... Types>
inline void log(Logger logger, Unicode_String format, Types const... args)
{
	Unicode_String str = format_unicode_string(logger.concat_allocator, format, args...);

	logger.log_proc(str);

	logger.concat_allocator.free(str.data, code_location());
}


inline Unicode_String mission_name = Unicode_String::empty;

template <typename... Types>
inline void abort_the_mission(Unicode_String format, Types... args)
{
	// assert(false);

	Unicode_String formatted = format_unicode_string(c_allocator, format, args...);
	
	wchar_t* message_box_title;
	if (mission_name.length)
	{
		message_box_title = (wchar_t*) mission_name.to_utf16(c_allocator, NULL);
	}
	else
	{
		message_box_title = (wchar_t*) u"Critical error";
	}

	#if OS_WINDOWS

	int utf16_length;
	MessageBoxW(NULL, (wchar_t*) formatted.to_utf16(c_allocator, &utf16_length), message_box_title, 0);

	#endif

	// Trigger the breakpoint

	exit(0);
}
