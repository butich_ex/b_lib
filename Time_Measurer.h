#pragma once
#include "b_lib/Basic.h"

#if OS_WINDOWS
#include "Windows.h"
#endif

struct Time_Measurer
{
	s64 freq_per_second;
	s64 last_us;

	inline s64 get_current_us()
	{
		LARGE_INTEGER counter;
		QueryPerformanceCounter(&counter);
		counter.QuadPart *= 1000000;

		return counter.QuadPart / freq_per_second;
	}

	inline s64 us_elapsed()
	{
		return get_current_us() - last_us;		
	}
	inline s64 ms_elapsed()
	{
		return us_elapsed() / 1000;
	}
	inline s64 seconds_elapsed()
	{
		return ms_elapsed() / 1000;
	}


	double ms_elapsed_double()
	{
		return double(us_elapsed()) / 1000.0;
	}
	double seconds_elapsed_double()
	{
		return ms_elapsed_double() / 1000.0;
	}

	void reset()
	{
		last_us = get_current_us();
	}
};

inline Time_Measurer create_time_measurer()
{
	Time_Measurer measurer;

	LARGE_INTEGER freq_li;
	QueryPerformanceFrequency(&freq_li);
	measurer.freq_per_second = freq_li.QuadPart;

	measurer.reset();

	return measurer;
}