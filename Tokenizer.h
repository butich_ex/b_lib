#pragma once

#include "File.h"
#include "Context.h"

struct Tokenizer
{
	File* file;

	Dynamic_Array<char> key_characters;

	Allocator allocator;

	Buffer token_buffer;

	int line;


	inline void skip_whitespace()
	{
		u8 b;
		while (file->read(&b))
		{
			if (!is_whitespace((char) b))
			{
				file->seek(-1);
				break;
			}
		}
	}

	inline String get_next_token()
	{
		token_buffer.occupied = 0;

		skip_whitespace();


		while (true)
		{
			char c;

			if (file->read((u8*) &c))
			{
				if (is_whitespace(c))
				{
					if (token_buffer.occupied == 0) continue;

					return String((char*) token_buffer.data, token_buffer.occupied);
				}

				if (key_characters.contains(c))
				{
					if (token_buffer.occupied == 0)
					{
						token_buffer.append(c);

						return String((char*) token_buffer.data, 1);
					}
					else
					{
						file->seek(-1);
						return String((char*) token_buffer.data, token_buffer.occupied);
					}
				}

				if (c == '\n')
				{
					if (token_buffer.occupied == 0)
					{
						line += 1;
						continue;
					}
					else
					{
						file->seek(-1);
						return String((char*) token_buffer.data, token_buffer.occupied);
					}
				}

				token_buffer.append(c);
			}
			else
			{
				return String::empty;
			}
		}
	}

	inline String peek_token()
	{
		if (token_buffer.occupied == 0)
		{
			return get_next_token();
		}

		return String((char*) token_buffer.data, token_buffer.occupied);
	}


	inline bool expect_token(String expected)
	{
		String token = peek_token();

		if (token != expected) return false;

		get_next_token();

		return true;
	}
};


inline Tokenizer tokenize(File* file, Allocator allocator)
{
	Tokenizer tokenizer;

	tokenizer.file = file;
	tokenizer.line = 0;
	tokenizer.key_characters = make_array<char>(32, allocator);
	tokenizer.allocator = allocator;

	tokenizer.token_buffer = create_buffer(32, allocator);

	return tokenizer;
}





// Tokenizer is set up specifically for this procedure.
inline bool read_thing_from_tokenizer_internal(Tokenizer* tokenizer, Reflection::Type* type, void* mem, Allocator temp_allocator, Allocator data_allocator)
{
	using namespace Reflection;

	switch (type->kind)
	{
		case Type_Kind::Primitive:
		{
			String token = tokenizer->peek_token();

			if (token.length == 0) return false;

			bool result = parse_primitive(token, (Primitive_Type*) type, mem);

			tokenizer->get_next_token();

			return result;
		}
		break;

		case Type_Kind::Enum:
		{
			Enum_Type* enum_type = (Enum_Type*) type;

			String token = tokenizer->peek_token();

			memset(mem, 0, enum_type->size);



			while (true)
			{
			loop_start_enum:

				String token = tokenizer->peek_token();

				auto memory_disjunction = [](u8* dst, u8* src, u64 size)
				{
					for (u64 i = 0; i < size; i++)
					{
						*dst |= *src;

						dst += 1;
						src += 1;
					}
				};



				for (Enum_Value enum_value : enum_type->values)
				{
					if (enum_value.name != token) continue;

					memory_disjunction((u8*) mem, (u8*) &enum_value.value, enum_type->size);

					token = tokenizer->get_next_token();
					if (token != "|") return true;

					tokenizer->get_next_token();

					goto loop_start_enum;
				}


				// Try to read number value if no corresponding named value found.
				Primitive_Value value;
				if (parse_primitive(token, enum_type->base_type, &value))
				{
					memory_disjunction((u8*) mem, (u8*) &value, enum_type->size);

					if (tokenizer->get_next_token() != "|")
					{
						return true;
					}

					goto loop_start_enum;
				}

				log(ctx.logger, U"Token '%' at line: % is not enum value name or number", token, tokenizer->line);

				return false;
			}

			assert(false);
		}
		break;

		case Type_Kind::String:
		{
			String_Type* string_type = (String_Type*) type;

			assert(string_type->string_kind == String_Kind::ASCII || string_type->string_kind == String_Kind::UTF32);



			Buffer buffer = create_buffer(64, temp_allocator);
			{
				String token = tokenizer->peek_token();
				if (token != "\"") return false;

				tokenizer->token_buffer.occupied = 0;


				bool met_backslash = false;
				while (true)
				{
					char c;
					if (!tokenizer->file->read((u8*) &c)) return false;

					if (c == '\"')
					{
						if (!met_backslash)
						{
							break;
						}
					}
					else if (c == '\\')
					{
						if (!met_backslash)
						{
							met_backslash = true;
							continue;
						}
					}

					buffer.append(c);

					met_backslash = false;
				}
			}


			if (string_type->string_kind == String_Kind::ASCII)
			{
				String str;
				str.data   = (char*) buffer.data;
				str.length = buffer.occupied;

				str = str.copy_with(data_allocator);

				memcpy(mem, &str, sizeof(String));
			}
			else
			{
				Unicode_String utf32 = Unicode_String::from_utf8((char*) buffer.data, data_allocator, buffer.occupied);


				memcpy(mem, &utf32, sizeof(Unicode_String));
			}

			temp_allocator.free(buffer.data, code_location());

			return true;
		}
		break;




		case Type_Kind::Struct:
		{
			Struct_Type* struct_type = (Struct_Type*) type;


			if (!tokenizer->expect_token("{")) return false;


			while (true)
			{
			loop_start_struct:

				String token = tokenizer->peek_token();

				if (token == "}")
				{
					tokenizer->get_next_token();
					return true;
				}


				for (auto iter = struct_type->iterate_members(temp_allocator); Struct_Member* member = iter.next();)
				{
					if (member->name != token) continue;

					tokenizer->get_next_token();

					if (!tokenizer->expect_token(":")) return false;

					if (!read_thing_from_tokenizer_internal(tokenizer, member->type, add_bytes_to_pointer(mem, member->offset), temp_allocator, data_allocator)) return false;

					goto loop_start_struct;
				}

				log(ctx.logger, U"Token '%' at line: % doesn't match to any member of struct '%'", token, tokenizer->line, struct_type->name);
				return false;
			}
		}
		break;


		case Type_Kind::Array:
		{
			Array_Type* array_type = (Array_Type*) type;


			if (!tokenizer->expect_token("[")) return false;

			u64 item_size = array_type->inner_type->size;
			assert(item_size != 0);


			Dynamic_Array<int> array;
			{
				array.capacity = 4;
				array.data = (int*) data_allocator.alloc(item_size * array.capacity, code_location());
				array.count = 0;
				array.allocator = data_allocator;
			}

			while (true)
			{
				String token = tokenizer->peek_token();

				if (token == "]")
				{
					tokenizer->get_next_token();
					break;
				}

				if (array.count > 0)
				{
					if (!tokenizer->expect_token(",")) break;
				}


				if (array.count == array.capacity)
				{
					array.capacity *= 2;
					array.data = (int*) data_allocator.realloc(array.data, item_size * array.capacity / 2, item_size * array.capacity, code_location());
				}


				if (!read_thing_from_tokenizer_internal(tokenizer, array_type->inner_type, add_bytes_to_pointer(array.data, item_size * array.count), temp_allocator, data_allocator))
				{
					log(ctx.logger, U"Failed to parse an array item at line: %. Index: %", tokenizer->line, array.count);
					temp_allocator.free(array.data, code_location());
					return false;
				}

				array.count += 1;

			}

			memcpy(mem, &array, sizeof(Dynamic_Array<int>));

			return true;
		}
		break;
	}
}


// This will push new token at the beginning itself.
inline bool read_thing(Tokenizer* tokenizer, Reflection::Type* type, void* mem, Allocator temp_allocator, Allocator data_allocator)
{
	Dynamic_Array<char> saved_key_characters = tokenizer->key_characters;

	const char tokens[] = "{}[]\":\\";

	Dynamic_Array<char> key_chars= make_array<char>(7, temp_allocator);
	key_chars.add_range(tokens, 7);

	tokenizer->key_characters = key_chars;

	tokenizer->get_next_token();

	bool result = read_thing_from_tokenizer_internal(tokenizer, type, mem, temp_allocator, data_allocator);

	key_chars.free();

	tokenizer->key_characters = saved_key_characters;

	return result;
}
